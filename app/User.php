<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'username',
        'address',
        'last_seen'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'last_seen',
        'born_date'
    ];

    public function admin()
    {
        return $this->morphedByMany(Admin::class, 'role');
    }

    public function schoolAdmin()
    {
        return $this->morphedByMany(SchoolAdmin::class, 'role');
    }

    public function teacher()
    {
        return $this->morphedByMany(Teacher::class, 'role');
    }

    public function student()
    {
        return $this->morphedByMany(Student::class, 'role');
    }
}
