<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'start_date',
        'end_date',
    ];

    public function submissions()
    {
        return $this->hasMany(AssignmentSubmission::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
