<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolAdmin extends Model
{
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function user()
    {
        return $this->morphToMany(User::class, 'role');
    }
}
