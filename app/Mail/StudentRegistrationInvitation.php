<?php

namespace App\Mail;

use App\Invitation;
use App\School;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentRegistrationInvitation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Invitation
     */
    public $invitation;

    /**
     * @var School
     */
    public $school;

    /**
     * Create a new message instance.
     *
     * InvitationToRegister constructor.
     * @param Invitation $invitation
     */
    public function __construct(Invitation $invitation, School $school)
    {
        $this->invitation = $invitation;
        $this->school = $school;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.student-register-invitation');
    }
}
