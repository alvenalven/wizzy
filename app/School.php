<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $guarded = ['status'];

    protected $fillable = [
        'npsn',
        'name',
        'address',
        'postal_code',
        'administrative_village',
        'district',
        'city',
        'province',
        'educational_level',
        'email',
        'phone',
        'website',
        'type',
    ];

    public function schoolAdmins()
    {
        return $this->hasMany(SchoolAdmin::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }

    public function classrooms()
    {
        return $this->hasMany(Classroom::class);
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }

    public function getRouteKeyName()
    {
        return 'npsn';
    }
}
