<?php

namespace App\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;

class Role implements Rule
{
    /**
     * Model names
     *
     * @var
     */
    protected $models;

    /**
     * Role constructor.
     * @param $models
     */
    public function __construct($models)
    {
        $this->models = $models;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach ($this->models as $model) {
            $user = $model::whereHas('user', function ($query) use ($value) {
                $query->where('username', $value)->orWhere('email', $value);
            })->first();

            if (isset($user)) return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tidak memiliki hak akses';
    }
}
