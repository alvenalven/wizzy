<?php

namespace App\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;

class isUser implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where('username', $value)->orWhere('email', $value)->first();

        return isset($user);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tidak ditemukan';
    }
}
