<?php

namespace App\Http\Middleware;

use App\Log;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class LastSeen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return $next($request);
        }

        $user = Auth::user();
        if ($user->student->first()) {
            if ($user->last_seen) {
                if (!$user->last_seen->isSameDay(Carbon::now()) || !$user->last_seen) {
                    $student = $user->student->first();
                    $expValue = 10;
                    $goldValue = 40;
                    Log::create([
                        'student_id' => $student->id,
                        'source' => 'login',
                        'type' => 'exp',
                        'value' => $expValue
                    ]);
                    Log::create([
                        'student_id' => $student->id,
                        'source' => 'login',
                        'type' => 'gold',
                        'value' => $goldValue
                    ]);
                    $student->exp += $expValue;
                    $newLevel = floor($student->exp / 100 + 1);
                    if ($newLevel > $student->level) {
                        $student->level = $newLevel;
                    }
                    $student->gold += $goldValue;
                    $student->save();
                }
            }
        }
        $user->update([
            'last_seen' => new \DateTime(),
        ]);

        return $next($request);
    }
}
