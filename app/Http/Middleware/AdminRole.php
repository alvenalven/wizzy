<?php

namespace App\Http\Middleware;

use Closure;

class AdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            if (auth()->user()->admin->first() === null && auth()->user()->schoolAdmin->first() === null) {
                return redirect()->route('login.index');
            }
        }
        else {
            return redirect()->route('login.index');
        }

        return $next($request);
    }
}
