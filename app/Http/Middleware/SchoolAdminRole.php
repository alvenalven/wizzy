<?php

namespace App\Http\Middleware;

use Closure;

class SchoolAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset(auth()->user()->admin)) {
            return redirect()->route('login.index');
        }

        return $next($request);
    }
}
