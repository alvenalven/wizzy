<?php

namespace App\Http\Requests;

use App\Rules\Role;
use App\Rules\isUser;
use Illuminate\Foundation\Http\FormRequest;

class StudentLogin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => [
                'bail',
                'required',
                new isUser(),
                new Role(['App\Student'])
            ],
            'password' => 'required',
        ];
    }

    /**
     * Set custom error message
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => 'Wajib diisi',
            'password.required' => 'Wajib diisi',
        ];
    }
}
