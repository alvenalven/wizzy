<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'      => 'bail|required|unique:users,username',
            'password'      => 'required',
            'conf_password' => 'bail|required|same:password'
        ];
    }

    /**
     * Set custom error message
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required'      => 'Wajib diisi',
            'username.unique'        => 'Tidak tersedia',
            'password.required'      => 'Wajib diisi',
            'conf_password.required' => 'Wajib diisi',
            'conf_password.same'     => 'Harus sama'
        ];
    }
}
