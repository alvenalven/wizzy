<?php

namespace App\Http\Controllers;

class NpsnController extends Controller
{
    public function show()
    {
        return view('npsn');
    }

    public function get()
    {
        $npsn = request('npsn');

        $url = 'http://referensi.data.kemdikbud.go.id/tabs.php?npsn=' . $npsn;

        $npsnSelector = '#tabs-1 table table td:contains("NPSN") + td + td';
        $nameSelector = '#tabs-1 table table td:contains("Nama") + td + td';
        $addressSelector = '#tabs-1 table table td:contains("Alamat") + td + td';
        $postalCodeSelector = '#tabs-1 table table td:contains("Kode Pos") + td + td';
        $administrativeVillageSelector = '#tabs-1 table table td:contains("Desa/Kelurahan") + td + td';
        $districtSelector = '#tabs-1 table table td:contains("Kecamatan/Kota (LN)") + td + td';
        $citySelector = '#tabs-1 table table td:contains("Kab.-Kota/Negara (LN)") + td + td';
        $provinceSelector = '#tabs-1 table table td:contains("Propinsi/Luar Negeri (LN)") + td + td';
        $statusSelector = '#tabs-1 table table td:contains("Status Sekolah") + td + td';
        $schoolHoursSelector = '#tabs-1 table table td:contains("Waktu Penyelenggaraan") + td + td';
        $educationalLevelSelector = '#tabs-1 table table td:contains("Jenjang Pendidikan") + td + td';
        $phoneSelector = '#tabs-6 tr td:contains("Fax") + td + td';
        $emailSelector = '#tabs-6 tr td:contains("Email") + td + td';
        $websiteSelector = '#tabs-6 tr td:contains("Website") + td + td';

        $crawler = \Scrapper::request('GET', $url);

        $npsn = trim($crawler->filter($npsnSelector)->text());
        $name = trim($crawler->filter($nameSelector)->text());
        $address = trim($crawler->filter($addressSelector)->text());
        $postalCode = trim($crawler->filter($postalCodeSelector)->text());
        $administrativeVillage = trim($crawler->filter($administrativeVillageSelector)->text());
        $district = trim($crawler->filter($districtSelector)->text());
        $city = trim($crawler->filter($citySelector)->text());
        $province = trim($crawler->filter($provinceSelector)->text());
        $status = trim($crawler->filter($statusSelector)->text());
        $schoolHours = trim($crawler->filter($schoolHoursSelector)->text());
        $educationalLevel = trim($crawler->filter($educationalLevelSelector)->text());
        $phone = trim($crawler->filter($phoneSelector)->text());
        $email = trim($crawler->filter($emailSelector)->text());
        $website = trim($crawler->filter($websiteSelector)->text());

        return compact('npsn', 'name', 'address', 'postalCode',
            'administrativeVillage', 'district', 'city', 'province', 'status', 'schoolHours',
            'educationalLevel', 'phone', 'email', 'website');
    }

    public function getList() {
        $keyword = trim(request()->keyword);
        $keyword = preg_replace('/\s+/', '+', $keyword);

        $url = 'http://referensi.data.kemdikbud.go.id/carisatpen.php?q=' . $keyword;

        $crawler = \Scrapper::request('GET', $url);
        $crawler = trim($crawler->filter('p')->html());
        $crawler = explode(PHP_EOL, $crawler);

        if (count($crawler) > 5) {
            $crawler = array_slice($crawler, 0, 5);
        }

        $keys = array('npsn', 'name', 'location');
        foreach ($crawler as $i => $item) {
            $item = explode('<br>', $item);
            $crawler[$i] = $item;
            $crawler[$i][1] = trim($crawler[$i][1]);
            $crawler[$i] = array_combine($keys, $crawler[$i]);
        }

        return $crawler;
    }
}
