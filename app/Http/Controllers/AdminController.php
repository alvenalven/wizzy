<?php

namespace App\Http\Controllers;

use App\Invitation;
use App\Mail\SchoolAdminRegisterInvitation;
use App\Role;
use App\School;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function runSchoolAdminView($type)
    {
        switch ($type) {
            case 'index':
                $view = app(SchoolAdminController::class)->index();
                return view($view['name'], $view['data']);
        }
    }

    public function showApprovedSchools()
    {
        $schools = School::where('status', 'approved')->with('schoolAdmins.user')->orderBy('created_at', 'desc')->paginate(5);
        foreach ($schools as $school) {
            $school->approved_at = $school->created_at->diffForHumans();
        }
        return view('admin.school', compact('schools'));
    }

    public function showPendingSchools()
    {
        if (\Auth::user()->schoolAdmin->first() !== null) {
            return $this->runSchoolAdminView('index');
        }

        $schools = School::where('status', 'pending')->with('schoolAdmins.user')->orderBy('created_at', 'desc')->paginate(5);
        foreach ($schools as $school) {
            $school->requested_at = $school->created_at->diffForHumans();
        }
        return view('admin.index', compact('schools'));
    }

    public function approve(School $school)
    {
        $school->status = 'approved';
        $school->created_at = Carbon::now();
        $school->save();

        $user = $school->schoolAdmins->first()->user->first();

        // create new invitation
        Invitation::create([
            'user_id' => $user->id,
            'type'    => 'register'
        ]);

        // send email for final registration process
        \Mail::to($user)->send(new SchoolAdminRegisterInvitation(Invitation::latest()->first()));

        return redirect()->back();
    }

    public function disapprove(School $school) {
        // send disapproval email here


        // cache user
        $user = $school->schoolAdmins->first()->user->first();

        // remove role
        Role::where('user_id', $user->id)->first()->delete();

        // remove school admin
        $school->schoolAdmins->first()->delete();

        // remove user
        $user->delete();

        // remove school
        $school->delete();

        return redirect()->back();
    }
}
