<?php

namespace App\Http\Controllers;

use App\Log;
use App\Material;
use App\MaterialDownload;
use App\Point;
use App\Student;
use App\StudentBadge;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class MaterialController extends Controller
{
    public function addMaterial()
    {
        // add file
        $path = request()->file('material')->storeAs(
            'materials', str_slug(request()->name) . '-' . md5(uniqid()) . '.' . request()->material->extension()
        );

        $material = new Material;
        $material->course_id = request()->courseId;
        $material->name = request()->name;
        $material->file = $path;
        $material->save();

        return redirect()->back();
    }

    public function deleteMaterial()
    {
        $material = Material::find(request()->materialId);

        // delete file
        Storage::delete($material->file);

        $material->delete();

        return redirect()->back();
    }

    public function downloadMaterial()
    {
        if (\Auth::user()->student->first()) {
            $materialDownload = MaterialDownload::where([
                'material_id' => request()->materialId,
                'student_id' => \Auth::user()->student->first()->id
            ])->get();
            if (count($materialDownload) == 0) {
                $material = Material::find(request()->materialId);
                $student = Student::find(\Auth::user()->student->first()->id);

                // value calculation
                $hoursDiff = Carbon::now()->diffInHours($material->created_at);
                if ($hoursDiff <= 23) {
                    // first 24 hours
                    $expValue = 40;
                    $goldValue = 200;
                    $pointReward = 100;
                }
                elseif ($hoursDiff <= 71) {
                    // first 72 hours
                    $expValue = 25;
                    $goldValue = 150;
                    $pointReward = 75;
                }
                elseif ($hoursDiff <= 167) {
                    // first 167 hours
                    $expValue = 20;
                    $goldValue = 125;
                    $pointReward = 60;
                }
                else {
                    // after a week
                    $expValue = 10;
                    $goldValue = 80;
                    $pointReward = 50;
                }

                // exp reward
                Log::create([
                    'student_id' => \Auth::user()->student->first()->id,
                    'course_id' => $material->course->id,
                    'source' => 'material',
                    'type' => 'exp',
                    'value' => $expValue
                ]);
                $student->exp += $expValue;
                $newLevel = floor($student->exp / 100 + 1);
                if ($newLevel > $student->level) {
                    $student->level = $newLevel;
                }
                $student->save();

                // gold reward
                Log::create([
                    'student_id' => \Auth::user()->student->first()->id,
                    'course_id' => $material->course->id,
                    'source' => 'material',
                    'type' => 'gold',
                    'value' => $goldValue
                ]);
                $student->gold += $goldValue;
                $student->save();

                // point reward
                Point::create([
                    'student_id' => \Auth::user()->student->first()->id,
                    'course_id' => $material->course->id,
                    'value' => $pointReward
                ]);

                // add material download data
                $download = new MaterialDownload;
                $download->material_id = request()->materialId;
                $download->student_id = \Auth::user()->student->first()->id;
                $download->save();

                // get own downloads
                $materialDownloads = MaterialDownload::where('student_id', \Auth::user()->student->first()->id)->get();

                // check is fastest
                $isFastest = count(MaterialDownload::where('material_id', request()->materialId)->get()) == 1;

                // check if fastest badge exists
                $isFastestBadgeExist = count(StudentBadge::where(['student_id' => \Auth::user()->student->first()->id, 'badge_id' => '10'])->get()) == 1;

                // check if acquire new badge
                if (count($materialDownloads) == 1) {
                    StudentBadge::create([
                        'student_id' => \Auth::user()->student->first()->id,
                        'badge_id' => 4
                    ]);
                }
                elseif (count($materialDownloads) == 5) {
                    StudentBadge::create([
                        'student_id' => \Auth::user()->student->first()->id,
                        'badge_id' => 5
                    ]);
                }
                elseif (count($materialDownloads) == 10) {
                    StudentBadge::create([
                        'student_id' => \Auth::user()->student->first()->id,
                        'badge_id' => 6
                    ]);
                }

                // check if acquire fastest badge
                if ($isFastest && !$isFastestBadgeExist) {
                    StudentBadge::create([
                        'student_id' => \Auth::user()->student->first()->id,
                        'badge_id' => 10
                    ]);
                }
            }
        }

        return Storage::download(Material::find(request()->materialId)->file);
    }
}