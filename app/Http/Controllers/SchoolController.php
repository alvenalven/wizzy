<?php

namespace App\Http\Controllers;

use App\Role;
use App\School;
use App\SchoolAdmin;
use App\User;

class SchoolController extends Controller
{
    public function store()
    {
        // add new school
        School::create(request()->all());

        // add new user
        $user = new User;
        $user->name = request()->admin_name;
        $user->email = request()->admin_email;
        $user->phone = request()->admin_phone;
        $user->save();

        // add new school admin
        $schoolAdmin = new SchoolAdmin;
        $schoolAdmin->school_id = School::max('id');
        $schoolAdmin->save();

        // add new role row
        $role = new Role;
        $role->user_id = User::max('id');
        $role->role_type = 'App\\SchoolAdmin';
        $role->role_id = SchoolAdmin::max('id');
        $role->save();
    }

    public function show(School $school)
    {
        return $school;
    }

    public function getSchoolStatus($npsn)
    {
        $school = School::where('npsn', $npsn)->first();


        return $school ? $school->status : null;
    }
}
