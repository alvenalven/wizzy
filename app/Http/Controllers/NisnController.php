<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

class NisnController extends Controller
{
    public function show()
    {
        return view('nisn');
    }

    public function get()
    {
        $nisn = '0012013528';

        $url = 'http://nisn.data.kemdikbud.go.id/page/data';
        $parameter = compact('nisn');

        $nameSelector = '#contentCenter_lRes1Nama';
        $genderSelector = '#contentCenter_lRes1Kelamin';
        $bornPlaceSelector = '#contentCenter_lRes1Tmptlahir';
        $bornDateSelector = '#contentCenter_lRes1TglLahir';

        $crawler = \Scrapper::request('POST', $url, $parameter);

        $name = $crawler->filter($nameSelector)->text();
        $gender = $crawler->filter($genderSelector)->text();
        $bornPlace = $crawler->filter($bornPlaceSelector)->text();
        $bornDate = $crawler->filter($bornDateSelector)->text();

        if (str_contains($bornDate, 'Januari')) {
            $bornDate = str_replace_first('Januari', 'January', $bornDate);
        } elseif (str_contains($bornDate, 'Februari')) {
            $bornDate = str_replace_first('Februari', 'February', $bornDate);
        } elseif (str_contains($bornDate, 'Maret')) {
            $bornDate = str_replace_first('Maret', 'March', $bornDate);
        } elseif (str_contains($bornDate, 'Mei')) {
            $bornDate = str_replace_first('Mei', 'May', $bornDate);
        } elseif (str_contains($bornDate, 'Juni')) {
            $bornDate = str_replace_first('Juni', 'June', $bornDate);
        } elseif (str_contains($bornDate, 'Juli')) {
            $bornDate = str_replace_first('Juli', 'July', $bornDate);
        } elseif (str_contains($bornDate, 'Agustus')) {
            $bornDate = str_replace_first('Agustus', 'August', $bornDate);
        } elseif (str_contains($bornDate, 'Oktober')) {
            $bornDate = str_replace_first('Oktober', 'October', $bornDate);
        } elseif (str_contains($bornDate, 'Desember')) {
            $bornDate = str_replace_first('Desember', 'December', $bornDate);
        }

        $bornDate = (Carbon::createFromFormat('d F Y', $bornDate, 'Asia/Jakarta')->format('Y-m-d'));

//        setlocale(LC_TIME, 'id');

//        dd(Carbon::create('9 Mei 2018')->formatLocalized('%d %B %Y'));

//        dd(Carbon::parse(date_format(Carbon::parse('9 Oktober 2018'), 'd F Y')));
//        dd((\DateTime::createFromFormat('%a', '2018 (UTC')));
//            dd(Carbon::getLocale());

//        return view('nisn', compact('name', 'gender', 'bornPlace', 'bornDate'));
        return view('schooladmin.addStudent', compact('nisn','name', 'gender', 'bornPlace', 'bornDate'));
        //uncomment below for json
//        return compact('name', 'gender', 'bornPlace', 'bornDate');
    }
}