<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\AssignmentSubmission;
use App\Log;
use App\Point;
use App\Student;
use App\StudentBadge;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AssignmentController extends Controller
{
    public function addAssignment()
    {
        // add file
        $path = request()->file('assignment')->storeAs(
            'assignments', str_slug(request()->name) . '-' . md5(uniqid()) . '.' . request()->assignment->extension()
        );

        $assignment = new Assignment;
        $assignment->course_id = request()->courseId;
        $assignment->name = request()->name;
        $assignment->file = $path;
        $assignment->start_date = request()->startDate;
        $assignment->end_date = request()->endDate;
        $assignment->end_date = $assignment->end_date->addDay()->subSecond();
        $assignment->save();

        return redirect()->back();
    }

    public function editAssignment()
    {
        $assignment = Assignment::find(request()->assignmentId);
        $assignment->start_date = request()->startDate;
        $assignment->end_date = request()->endDate;
        $assignment->end_date = $assignment->end_date->addDay()->subSecond();
        $assignment->save();

        return redirect()->back();
    }

    public function deleteAssignment()
    {
        $assignment = Assignment::find(request()->assignmentId);

        // delete file
        Storage::delete($assignment->file);

        $assignment->delete();

        return redirect()->back();
    }

    public function downloadAssignment()
    {
        return Storage::download(Assignment::find(request()->assignmentId)->file);
    }

    public function downloadAssignmentSubmission()
    {
        return Storage::download(AssignmentSubmission::find(request()->assignmentSubmissionId)->file);
    }

    public function submitAssignment()
    {
        // add file
        $path = request()->file('submission')->storeAs(
            'submissions', str_slug(pathinfo(request()->submission->getClientOriginalName(), PATHINFO_FILENAME)) . '-' . md5(uniqid()) . '.' . request()->submission->extension()
        );

        // add assignment submission
        $submission = new AssignmentSubmission;
        $submission->assignment_id = request()->assignmentId;
        $submission->student_id = \Auth::user()->student->first()->id;
        $submission->file = $path;
        $submission->save();

        // value calculation
        $assignment = Assignment::find(request()->assignmentId);
        $student = Student::find(\Auth::user()->student->first()->id);
        $assignmentDuration = $assignment->start_date->diffInHours($assignment->end_date);
        $quarterDuration = (int) floor(0.25 * $assignmentDuration);
        $halfDuration = (int) floor(0.5 * $assignmentDuration);
        $lastQuarterDuration = (int) floor(0.75 * $assignmentDuration);
        $hoursDiff = Carbon::now()->diffInHours($assignment->start_date);
        if ($hoursDiff <= $quarterDuration) {
            $expValue = 80;
            $goldValue = 400;
            $pointReward = 200;
        }
        elseif ($hoursDiff <= $halfDuration) {
            $expValue = 50;
            $goldValue = 300;
            $pointReward = 150;
        }
        elseif ($hoursDiff <= $lastQuarterDuration) {
            $expValue = 40;
            $goldValue = 250;
            $pointReward = 120;
        }
        else {
            $expValue = 20;
            $goldValue = 160;
            $pointReward = 100;
        }

        // exp reward
        Log::create([
            'student_id' => $student->id,
            'course_id' => $assignment->course->id,
            'source' => 'assignment',
            'type' => 'exp',
            'value' => $expValue
        ]);
        $student->exp += $expValue;
        $newLevel = floor($student->exp / 100 + 1);
        if ($newLevel > $student->level) {
            $student->level = $newLevel;
        }
        $student->save();

        // gold reward
        Log::create([
            'student_id' => $student->id,
            'course_id' => $assignment->course->id,
            'source' => 'assignment',
            'type' => 'gold',
            'value' => $goldValue
        ]);
        $student->gold += $goldValue;
        $student->save();

        // point reward
        Point::create([
            'student_id' => $student->id,
            'course_id' => $assignment->course->id,
            'value' => $pointReward
        ]);

        // get own assignment submissions
        $ownSubmissions = AssignmentSubmission::where('student_id', $student->id)->get();

        // get own fast assignment submissions (< 50% time)
        $fastSubmissionCount = 0;
        $ownSubmissionLogs = Log::where([
            'student_id' => $student->id,
            'source' => 'assignment',
            'type' => 'exp'
        ])->get();
        foreach ($ownSubmissionLogs as $ownSubmissionLog) {
            if ($ownSubmissionLog->value == '50') {
                $fastSubmissionCount++;
            }
        }

        // check is fastest
        $isFastest = count(AssignmentSubmission::where('assignment_id', request()->assignmentId)->get()) == 1;

        // check if fastest badge exists
        $isFastestBadgeExist = count(StudentBadge::where(['student_id' => \Auth::user()->student->first()->id, 'badge_id' => '9'])->get()) == 1;

        // check if acquire new badge
        if (count($ownSubmissions) == 1) {
            StudentBadge::create([
                'student_id' => \Auth::user()->student->first()->id,
                'badge_id' => 1
            ]);
        }
        elseif (count($ownSubmissions) == 5) {
            StudentBadge::create([
                'student_id' => \Auth::user()->student->first()->id,
                'badge_id' => 2
            ]);
        }
        elseif (count($ownSubmissions) == 10) {
            StudentBadge::create([
                'student_id' => \Auth::user()->student->first()->id,
                'badge_id' => 3
            ]);
        }

        // check if acquire fastest badge
        if ($isFastest && !$isFastestBadgeExist) {
            StudentBadge::create([
                'student_id' => \Auth::user()->student->first()->id,
                'badge_id' => 9
            ]);
        }

        // check if acquire multi fast submission (50%) badge
        if ($fastSubmissionCount == 5) {
            StudentBadge::create([
                'student_id' => \Auth::user()->student->first()->id,
                'badge_id' => 7
            ]);
        }
        elseif ($fastSubmissionCount == 10) {
            StudentBadge::create([
                'student_id' => \Auth::user()->student->first()->id,
                'badge_id' => 8
            ]);
        }

        return redirect()->back();
    }
}