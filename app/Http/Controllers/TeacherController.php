<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\AssignmentSubmission;
use App\Course;
use App\Material;
use App\Schedule;
use App\Shift;
use App\StudentGrade;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class TeacherController extends Controller
{
    public function getTeachersFromRemote()
    {
        $url = 'http://cari.padamu.siap.web.id/cari';

        $key = request()->key;
        $limit = request()->limit;
        $kemdikbud = 0;

        $parameter = compact('key', 'limit', 'kemdikbud');

        $client = new Client();
        $result = $client->post($url, [
            'form_params' => $parameter
        ])->getBody();
        $result = json_decode($result);

        $teachers = [];

        foreach ($result->result as $key => $teacher) {
            $teachers[$key] = new \stdClass();
            $teachers[$key]->nuptk = $teacher->ptk->nuptk;
            $teachers[$key]->ptk_id = $teacher->ptk->ptk_id;
            $teachers[$key]->name = ucwords(strtolower($teacher->ptk->nama));
            $teachers[$key]->gender = $teacher->ptk->kelamin === 'L' ? 'Male' : 'Female';
        }

        return $teachers;
    }

    public function showTeacherDashboard()
    {
        $totalAssignment = 0;
        $totalOngoingAssignment = 0;
        $currentSchedule = null;
        $nextSchedule = array();
        $problematicStudents = array();

        foreach (\Auth::user()->teacher->first()->courses as $course) {
            $totalAssignment += count($course->assignments);

            foreach ($course->assignments as $assignment) {
                if (Carbon::now()->gte($assignment->start_date) && Carbon::now()->lt($assignment->end_date)) {
                    $totalOngoingAssignment += 1;
                }
            }

            if (count($course->schedules) > 0) {
                foreach ($course->schedules as $schedule) {
                    // if same day
                    if (Carbon::now()->dayOfWeekIso == $schedule->day_of_week) {
                        // start time
                        $startTime = explode(':', $schedule->shift->start_time);
                        foreach ($startTime as $index => $time) {
                            $startTime[$index] = intval($startTime[$index]);
                        }
                        $startTime = Carbon::create(Carbon::now()->year, Carbon::now()->month, Carbon::now()->day, $startTime[0], $startTime[1], $startTime[2]);
                        // end time
                        $endTime = explode(':', $schedule->shift->end_time);
                        foreach ($endTime as $index => $time) {
                            $endTime[$index] = intval($endTime[$index]);
                        }
                        $endTime = Carbon::create(Carbon::now()->year, Carbon::now()->month, Carbon::now()->day, $endTime[0], $endTime[1], $endTime[2]);

                        // if now
                        if (Carbon::now()->gte($startTime) && Carbon::now()->lt($endTime)) {
                            $currentSchedule = $schedule;
                        }
                        // if later
                        else {
                            if (Carbon::now()->lt($startTime)) {
                                array_push($nextSchedule, $schedule);
                            }
                        }
                    }
                }
            }

            // get problematic students
            $classroomStudents = $course->classroom->students;
            foreach ($course->assignments as $assignment) {
                foreach ($classroomStudents as $classroomStudent)
                $hasDoneAssignment = count(AssignmentSubmission::where([
                    'student_id' => $classroomStudent->id,
                    'assignment_id' => $assignment->id
                ])->get()) > 0;
                if (!$hasDoneAssignment) {
                    $classroomStudent->problematic_course = $course;
                    $classroomStudent->problematic_assignment = $assignment;
                    array_push($problematicStudents, $classroomStudent);
                }
            }
        }

        $problematicStudents = array_unique($problematicStudents);

        return view('teacher.index', compact('totalAssignment', 'totalOngoingAssignment', 'currentSchedule', 'nextSchedule', 'problematicStudents'));
    }

    public function showSubmissions(Assignment $assignment)
    {
        $submissions = $assignment->submissions;
        return $submissions;
    }

    public function showSchedulePage()
    {
        $shifts = Shift::where('school_id', \Auth::user()->teacher()->first()->school->id)->get();
        $schedules = array();
        foreach (\Auth::user()->teacher()->first()->courses as $course) {
            foreach ($course->schedules as $schedule) {
                array_push($schedules, $schedule);
            }
        }

        return view('teacher.schedule', compact('shifts', 'schedules'));
    }

    public function showStudentsPage(Course $course)
    {
        $students = $course->classroom->students()->with('user')->orderBy('created_at', 'desc')->paginate(5);
        return view('teacher.student', compact('students', 'course'));
    }

    public function showAssignmentPage(Course $course)
    {
        $assignments = Assignment::where('course_id', $course->id)->get();
        return view('teacher.assignment', compact('assignments', 'course'));
    }

    public function showMaterialPage(Course $course)
    {
        $materials = Material::where('course_id', $course->id)->get();
        return view('teacher.material', compact('materials', 'course'));
    }

    public function showGradeInput(Course $course)
    {
        $students = $course->classroom->students;
        $hasFilledGrade = count(StudentGrade::where([
            'student_id' => $students->first()->id,
            'course_id' => $course->id
        ])->get()) == 1;

        return view('teacher.grade', compact('students', 'course', 'hasFilledGrade'));
    }

    public function inputGrade()
    {
        $course = Course::find(request()->courseId);
        $students = $course->classroom->students;

        foreach ($students as $student) {
            StudentGrade::create([
                'student_id' => $student->id,
                'course_id' => request()->courseId,
                'mid' => request()->mid[$student->id],
                'final' => request()->final[$student->id]
            ]);
        }

        return redirect()->back();
    }
}
