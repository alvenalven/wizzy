<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLogin;
use App\Http\Requests\StudentLogin;
use App\Http\Requests\TeacherLogin;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param AdminLogin $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function authenticate(AdminLogin $request)
    {
        $usernameLogin = [
            'username' => $request->username,
            'password' => $request->password
        ];

        $emailLogin = [
            'email'    => $request->username,
            'password' => $request->password
        ];

        if (\Auth::attempt($usernameLogin) || \Auth::attempt($emailLogin)) {
            return redirect()->intended(route('admin.index'));
        }
        else {
            return back()->withErrors(['password' => 'Tidak sesuai'])->withInput(['username' => $request->username]);
        }
    }

    public function authenticateTeacher(TeacherLogin $request)
    {
        $usernameLogin = [
            'username' => $request->username,
            'password' => $request->password
        ];

        $emailLogin = [
            'email'    => $request->username,
            'password' => $request->password
        ];

        if (\Auth::attempt($usernameLogin) || \Auth::attempt($emailLogin)) {
            return redirect()->intended(route('teacher.index'));
        }
        else {
            return back()->withErrors(['password' => 'Tidak sesuai'])->withInput(['username' => $request->username]);
        }
    }

    public function authenticateStudent(StudentLogin $request)
    {
        $usernameLogin = [
            'username' => $request->username,
            'password' => $request->password
        ];

        $emailLogin = [
            'email'    => $request->username,
            'password' => $request->password
        ];

        if (\Auth::attempt($usernameLogin) || \Auth::attempt($emailLogin)) {
            return redirect()->intended(route('student.index'));
        }
        else {
            return back()->withErrors(['password' => 'Tidak sesuai'])->withInput(['username' => $request->username]);
        }
    }

    public function showAdminLoginForm()
    {
        return view('admin.login');
    }

    public function adminLogout()
    {
        \Auth::logout();
        return $this->showAdminLoginForm();
    }

    public function showTeacherLoginForm()
    {
        return view('teacher.login');
    }

    public function teacherLogout()
    {
        \Auth::logout();
        return $this->showTeacherLoginForm();
    }

    public function showStudentLoginForm()
    {
        return view('student.login');
    }

    public function studentLogout()
    {
        \Auth::logout();
        return $this->showStudentLoginForm();
    }
}