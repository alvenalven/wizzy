<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUser;
use App\Invitation;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function showRegisterForm(Invitation $invitation)
    {
        $user = $invitation->user;
        $code = $invitation->code;
        return view('guest.register', compact('user', 'code'));
    }

    public function completeUserRegistration(RegisterUser $request, Invitation $invitation)
    {
         // update user
        $user = $invitation->user;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->save();

        // remove invitation
        $invitation->delete();

        // login to account
        Auth::login($user);

        // redirect to account panel route
        if ($user->schoolAdmin->first()) {
            return redirect()->route('admin.index');
        }
        // else if other roles
    }
}
