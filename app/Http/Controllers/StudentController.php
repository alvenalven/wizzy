<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\AssignmentSubmission;
use App\Badge;
use App\ClassroomStudent;
use App\Course;
use App\Log;
use App\Material;
use App\Point;
use App\Shift;
use App\StudentBadge;
use App\StudentGrade;
use App\StudentItem;
use Carbon\Carbon;

class StudentController extends Controller
{
    public function getStudentsFromRemote()
    {
        $nisn = request()->nisn;

        $url = 'http://nisn.data.kemdikbud.go.id/page/data';
        $parameter = compact('nisn');

        $nameSelector = '#contentCenter_lRes1Nama';
        $genderSelector = '#contentCenter_lRes1Kelamin';
        $bornPlaceSelector = '#contentCenter_lRes1Tmptlahir';
        $bornDateSelector = '#contentCenter_lRes1TglLahir';

        $crawler = \Scrapper::request('POST', $url, $parameter);

        $name = ucwords(strtolower($crawler->filter($nameSelector)->text()));
        $gender = $crawler->filter($genderSelector)->text();
        if ($gender === 'Laki Laki' || $gender == 1) {
            $gender = 'Male';
        } elseif ($gender === 'Perempuan' || $gender == 2) {
            $gender = 'Female';
        }
        $bornPlace = $crawler->filter($bornPlaceSelector)->text();
        $bornDate = $crawler->filter($bornDateSelector)->text();

        if (str_contains($bornDate, 'Januari')) {
            $bornDate = str_replace_first('Januari', 'January', $bornDate);
        } elseif (str_contains($bornDate, 'Februari')) {
            $bornDate = str_replace_first('Februari', 'February', $bornDate);
        } elseif (str_contains($bornDate, 'Maret')) {
            $bornDate = str_replace_first('Maret', 'March', $bornDate);
        } elseif (str_contains($bornDate, 'Mei')) {
            $bornDate = str_replace_first('Mei', 'May', $bornDate);
        } elseif (str_contains($bornDate, 'Juni')) {
            $bornDate = str_replace_first('Juni', 'June', $bornDate);
        } elseif (str_contains($bornDate, 'Juli')) {
            $bornDate = str_replace_first('Juli', 'July', $bornDate);
        } elseif (str_contains($bornDate, 'Agustus')) {
            $bornDate = str_replace_first('Agustus', 'August', $bornDate);
        } elseif (str_contains($bornDate, 'Oktober')) {
            $bornDate = str_replace_first('Oktober', 'October', $bornDate);
        } elseif (str_contains($bornDate, 'Desember')) {
            $bornDate = str_replace_first('Desember', 'December', $bornDate);
        }

        $bornDate = (Carbon::createFromFormat('d F Y', $bornDate, 'Asia/Jakarta')->format('Y-m-d'));

        return compact('name', 'gender', 'bornPlace', 'bornDate');
    }

    public function showStudentDashboard()
    {
        $currentSchedule = null;
        $todaySchedule = array();
        $tomorrowSchedule = array();
        $classroomStudents = array();
        $classroomStudentsFinal = array();

        $assignments = array();

        $classes = ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->get();

        foreach ($classes as $class) {
            foreach ($class->classroom->courses as $course) {
                foreach ($course->schedules as $schedule) {
                    // if same day
                    if (Carbon::now()->dayOfWeekIso == $schedule->day_of_week) {
                        // start time
                        $startTime = explode(':', $schedule->shift->start_time);
                        foreach ($startTime as $index => $time) {
                            $startTime[$index] = intval($startTime[$index]);
                        }
                        $startTime = Carbon::create(Carbon::now()->year, Carbon::now()->month, Carbon::now()->day, $startTime[0], $startTime[1], $startTime[2]);
                        // end time
                        $endTime = explode(':', $schedule->shift->end_time);
                        foreach ($endTime as $index => $time) {
                            $endTime[$index] = intval($endTime[$index]);
                        }
                        $endTime = Carbon::create(Carbon::now()->year, Carbon::now()->month, Carbon::now()->day, $endTime[0], $endTime[1], $endTime[2]);

                        // if now
                        if (Carbon::now()->gte($startTime) && Carbon::now()->lt($endTime)) {
                            $currentSchedule = $schedule;
                        }
                        // if not now
                        else {
                            array_push($todaySchedule, $schedule);
                        }
                    }
                    // if tomorrow
                    elseif (Carbon::now()->dayOfWeekIso + 1 == $schedule->day_of_week) {
                        // push to tomorrow schedule array
                        array_push($tomorrowSchedule, $schedule);
                    }
                }
                // get ongoing assignment list
                foreach ($course->assignments as $assignment) {
                    if (Carbon::now()->lte($assignment->end_date)) {
                        $hasSubmit = count(AssignmentSubmission::where(['student_id' => \Auth::user()->student()->first()->id, 'assignment_id' => $assignment->id])->get()) == 1;
                        $assignment->has_submit = $hasSubmit;
                        array_push($assignments, $assignment);
                    }
                }
            }
            // get classroom students
            foreach ($class->classroom->students as $singleStudent) {
                $singleStudent->total_point = Point::where('student_id', $singleStudent->id)->sum('value');
                $singleStudent->name = $singleStudent->user()->first()->name;
                array_push($classroomStudents, $singleStudent);
            }
        }

        foreach ($classroomStudents as $singleStudent) {
            array_push($classroomStudentsFinal, $singleStudent->toArray());
        }

        $classroomStudentsFinal = array_values(array_sort($classroomStudentsFinal, function ($value) {
            return $value['total_point'];
        }));
        $classroomStudentsFinal = array_reverse($classroomStudentsFinal, true);
        $classroomStudentsFinal = array_slice($classroomStudentsFinal, 0, 5);

        // get badges list
        $badges = \Auth::user()->student()->first()->badges;

        return view('student.index', compact('currentSchedule', 'todaySchedule', 'tomorrowSchedule', 'assignments', 'classroomStudentsFinal', 'badges'));
    }

    public function showSchedulePage()
    {
        $schedules = array();
        $shifts = Shift::where('school_id', \Auth::user()->student()->first()->school->id)->get();

        $classes = ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->get();
        foreach ($classes as $class) {
            foreach ($class->classroom->courses as $course) {
                foreach ($course->schedules as $schedule) {
                    array_push($schedules, $schedule);
                }
            }
        }

        return view('student.schedule', compact('shifts', 'schedules'));
    }

    public function showMaterialPage(Course $course)
    {
        $courses = array();
        $materials = Material::where('course_id', $course->id)->get();

        $classes = ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->get();
        foreach ($classes as $class) {
            foreach ($class->classroom->courses as $singleCourse) {
                array_push($courses, $singleCourse);
            }
        }

        return view('student.material', compact('materials', 'course', 'courses'));
    }

    public function showAssignmentPage(Course $course)
    {
        $courses = array();

        $assignments = Assignment::where('course_id', $course->id)->get();
        $classes = ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->get();
        foreach ($classes as $class) {
            foreach ($class->classroom->courses as $singleCourse) {
                array_push($courses, $singleCourse);
            }
        }
        foreach ($assignments as $assignment) {
            $submission = AssignmentSubmission::where([
                'assignment_id' => $assignment->id,
                'student_id' => \Auth::user()->student->first()->id
            ])->get();
            $assignment->is_submitted = count($submission) > 0;
        }

        return view('student.assignment', compact('assignments', 'course', 'courses'));
    }

    public function showXpLog()
    {
        $studentLogs = \Auth::user()->student->first->logs;
        $logs = array();

        foreach ($studentLogs->logs as $log) {
            if ($log->type == 'exp') {
                array_push($logs, $log);
            }
        }

        $logs = array_reverse($logs);

        return view('student.xp', compact('logs'));
    }

    public function showGoldLog()
    {
        $studentLogs = \Auth::user()->student->first->logs;
        $logs = array();

        foreach ($studentLogs->logs as $log) {
            if ($log->type == 'gold') {
                array_push($logs, $log);
            }
        }

        $logs = array_reverse($logs);

        return view('student.gold', compact('logs'));
    }

    public function showBadgeLog()
    {
        $badges = Badge::all();
        foreach ($badges as $badge) {
            $studentBadge = StudentBadge::where([
                'student_id' => \Auth::user()->student->first()->id,
                'badge_id' => $badge->id
            ])->get();
            $hasBadge = count($studentBadge) == 1;
            $badge->has_badge = $hasBadge;
            if ($hasBadge) {
                $badge->created_at = $studentBadge->first()->created_at;
            }
            else {
                $badge->created_at = NULL;
            }
        }

        // get own fast assignment submissions (< 50% time)
        $fastSubmissionCount = 0;
        $ownSubmissionLogs = Log::where([
            'student_id' => \Auth::user()->student->first()->id,
            'source' => 'assignment',
            'type' => 'exp'
        ])->get();
        foreach ($ownSubmissionLogs as $ownSubmissionLog) {
            if ($ownSubmissionLog->value == '50') {
                $fastSubmissionCount++;
            }
        }

        return view('student.badge', compact('badges', 'fastSubmissionCount'));
    }

    public function showShop()
    {
        return view('student.shop');
    }

    public function buyShopItem()
    {
        $student = \Auth::user()->student->first();
        if ($student->gold >= 1000) {
            StudentItem::create([
                'item_id' => request()->itemId,
                'student_id' => $student->id
            ]);
            Log::create([
                'student_id' => $student->id,
                'source' => 'shop',
                'type' => 'gold',
                'value' => 1000
            ]);
            $student->gold -= 1000;
            $student->save();
        }

        return redirect()->back();
    }

    public function showTheme()
    {
        $items = StudentItem::where('student_id', \Auth::user()->student()->first()->id)->get();
        return view('student.theme', compact('items'));
    }

    public function applyTheme()
    {
        $student = \Auth::user()->student->first();
        if (request()->itemId == 0) {
            $student->color_scheme = 'wizzy';
        }
        elseif (request()->itemId == 1) {
            $student->color_scheme = 'winter-mist';
        }
        elseif (request()->itemId == 2) {
            $student->color_scheme = 'tropical-punch';
        }
        $student->save();

        return redirect()->back();
    }

    public function showProfile()
    {
        return view('student.profile');
    }

    public function showEditProfile()
    {
        return view('student.profile-edit');
    }

    public function editProfile()
    {
        $user = \Auth::user();

        // add file
        if (request()->file('photo')) {
            $path = request()->file('photo')->storeAs(
                'profile', str_slug($user->name) . '-' . md5(uniqid()) . '.' . request()->photo->extension(), 'public-images'
            );
            $user->photo = $path;
        }

        $user->address = request()->address;
        $user->email = request()->email;
        $user->phone = request()->phone;
        $user->save();

        return redirect()->route('student.showProfile');
    }

    public function showGrade()
    {
        $studentGrades = StudentGrade::where('student_id', \Auth::user()->student()->first()->id)->get();

        return view('student.grade', compact('studentGrades'));
    }
}
