<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\ClassroomStudent;
use App\Course;
use App\Invitation;
use App\Mail\StudentRegistrationInvitation;
use App\Mail\TeacherRegistrationInvitation;
use App\Role;
use App\Schedule;
use App\SchoolAdmin;
use App\Shift;
use App\Student;
use App\Subject;
use App\Teacher;
use App\User;

class SchoolAdminController extends Controller
{
    public function index()
    {
        $school = \Auth::user()->schoolAdmin()->first()->school;

        return [
            'name' => 'school-admin.index',
            'data' => compact('school')
        ];
    }

    public function showUpdateSchoolForm()
    {
        $school = \Auth::user()->schoolAdmin()->first()->school;
        return view('school-admin.update-school', compact('school'));
    }

    public function updateSchool()
    {
        $school = \Auth::user()->schoolAdmin()->first()->school;
        $school->update([
            'educational_level' => request()->educationalLevel,
            'type' => request()->type,
            'address' => request()->address,
            'postal_code' => request()->postalCode,
            'administrative_village' => request()->administrativeVillage,
            'district' => request()->district,
            'city' => request()->city,
            'province' => request()->province,
            'email' => request()->email,
            'website' => request()->website,
            'phone' => request()->phone,
        ]);
        return back();
    }

    public function showTeachersPage()
    {
        $teachers = \Auth::user()->schoolAdmin()->first()->school->teachers()
            ->with('user', 'user.schoolAdmin')->orderBy('created_at', 'desc')->paginate(5);
        return view('school-admin.teacher', compact('teachers'));
    }

    public function showActiveTeachersPage()
    {
        $teachers = \Auth::user()->schoolAdmin()->first()->school->teachers()->whereHas('user', function ($query) {
            $query->whereNotNull('username');
        })->with('user', 'user.schoolAdmin')->orderBy('created_at', 'desc')->paginate(5);
        return view('school-admin.teacher', compact('teachers'));
    }

    public function showPendingTeachersPage()
    {
        $teachers = \Auth::user()->schoolAdmin()->first()->school->teachers()->whereHas('user', function ($query) {
            $query->whereNull('username');
        })->with('user', 'user.schoolAdmin')->orderBy('created_at', 'desc')->paginate(5);
        return view('school-admin.teacher', compact('teachers'));
    }

    public function inviteTeacher($teacher)
    {
        // add new user
        $user = new User;
        $user->name = $teacher['name'];
        $user->email = $teacher['email'];
        $user->gender = $teacher['gender'];
        $user->save();

        // add new teacher
        $teacherModel = new Teacher;
        $teacherModel->school_id = \Auth::user()->schoolAdmin()->first()->school->id;
        $teacherModel->nuptk = $teacher['nuptk'];
        $teacherModel->ptk_id = $teacher['ptk_id'];
        $teacherModel->save();

        // add new role
        $role = new Role;
        $role->user_id = User::max('id');
        $role->role_type = 'App\\Teacher';
        $role->role_id = Teacher::max('id');
        $role->save();

        // create new invitation
        Invitation::create([
            'user_id' => User::max('id'),
            'type' => 'register'
        ]);

        // send invitation
        \Mail::to($user)->send(new TeacherRegistrationInvitation(Invitation::latest()->first(), \Auth::user()->schoolAdmin()->first()->school));
    }

    public function submitTeacherInvitation()
    {
        $teachers = request()->teachers;
        foreach ($teachers as $teacher) {
            $this->inviteTeacher($teacher);
        }
    }

    public function showStudentsPage()
    {
        $students = \Auth::user()->schoolAdmin()->first()->school->students()
            ->with('user')->orderBy('created_at', 'desc')->paginate(5);
        return view('school-admin.student', compact('students'));
    }

    public function showActiveStudentsPage()
    {
        $students = \Auth::user()->schoolAdmin()->first()->school->students()->whereHas('user', function ($query) {
            $query->whereNotNull('username');
        })->with('user')->orderBy('created_at', 'desc')->paginate(5);
        return view('school-admin.student', compact('students'));
    }

    public function showPendingStudentsPage()
    {
        $students = \Auth::user()->schoolAdmin()->first()->school->students()->whereHas('user', function ($query) {
            $query->whereNull('username');
        })->with('user')->orderBy('created_at', 'desc')->paginate(5);
        return view('school-admin.student', compact('students'));
    }

    public function inviteStudent($student)
    {
        // add new user
        $user = new User;
        $user->name = $student['name'];
        $user->email = $student['email'];
        $user->gender = $student['gender'];
        $user->born_place = $student['bornPlace'];
        $user->born_date = $student['bornDate'];
        $user->save();

        // add new student
        $studentModel = new Student;
        $studentModel->school_id = \Auth::user()->schoolAdmin()->first()->school->id;
        $studentModel->nisn = $student['nisn'];
        $studentModel->save();

        // add new role
        $role = new Role;
        $role->user_id = User::max('id');
        $role->role_type = 'App\\Student';
        $role->role_id = Student::max('id');
        $role->save();

        // create new invitation
        Invitation::create([
            'user_id' => User::max('id'),
            'type' => 'register'
        ]);

        // send invitation
        \Mail::to($user)->send(new StudentRegistrationInvitation(Invitation::latest()->first(), \Auth::user()->schoolAdmin()->first()->school));
    }

    public function submitStudentInvitation()
    {
        $students = request()->students;
        foreach ($students as $student) {
            $this->inviteStudent($student);
        }
    }

    public function resendTeacherInvitation()
    {
        $user = User::find(request()->userId);
        $invitation = Invitation::where('user_id', request()->userId)->get();
        if (!empty($invitation->toArray())) {
            \Mail::to($user)->send(new TeacherRegistrationInvitation($invitation->first(), \Auth::user()->schoolAdmin()->first()->school));
        }
    }

    public function cancelTeacherInvitation()
    {
        $user = User::find(request()->userId);

        // delete invitation
        Invitation::where('user_id', request()->userId)->get()->first()->delete();

        // delete teacher
        $user->teacher->first()->delete();

        // delete role
        Role::where('user_id', request()->userId)->get()->first()->delete();

        // delete user
        $user->delete();
    }

    public function grantSchoolAdminRole()
    {
        // create school admin
        $schoolAdmin = new SchoolAdmin;
        $schoolAdmin->school_id = \Auth::user()->schoolAdmin()->first()->school->id;
        $schoolAdmin->save();

        // create role
        $role = new Role;
        $role->user_id = request()->userId;
        $role->role_type = 'App\\SchoolAdmin';
        $role->role_id = SchoolAdmin::max('id');
        $role->save();
    }

    public function resendStudentInvitation()
    {
        $user = User::find(request()->userId);
        $invitation = Invitation::where('user_id', request()->userId)->get();
        if (!empty($invitation->toArray())) {
            \Mail::to($user)->send(new StudentRegistrationInvitation($invitation->first(), \Auth::user()->schoolAdmin()->first()->school));
        }
    }

    public function cancelStudentInvitation()
    {
        $user = User::find(request()->userId);

        // delete invitation
        Invitation::where('user_id', request()->userId)->get()->first()->delete();

        // delete student
        $user->student->first()->delete();

        // delete role
        Role::where('user_id', request()->userId)->get()->first()->delete();

        // delete user
        $user->delete();
    }

    public function showClassList()
    {
        $classes = Classroom::where('school_id', \Auth::user()->schoolAdmin()->first()->school->id)->get();
        return view('school-admin.class', compact('classes'));
    }

    public function addClassroom()
    {
        $class = new Classroom;
        $class->school_id = \Auth::user()->schoolAdmin()->first()->school->id;
        $class->name = request()->name;
        $class->save();

        return redirect()->back();
    }

    public function editClassroom()
    {
        $class = Classroom::find(request()->id);
        $class->name = request()->name;
        $class->save();

        return redirect()->back();
    }

    public function deleteClassroom()
    {
        $class = Classroom::find(request()->id);
        $class->delete();

        return redirect()->back();
    }

    public function showSubjectList()
    {
        $subjects = Subject::where('school_id', \Auth::user()->schoolAdmin()->first()->school->id)->get();
        return view('school-admin.subject', compact('subjects'));
    }

    public function addSubject()
    {
        $subject = new Subject;
        $subject->school_id = \Auth::user()->schoolAdmin()->first()->school->id;
        $subject->name = request()->name;
        $subject->save();

        return redirect()->back();
    }

    public function editSubject()
    {
        $subject = Subject::find(request()->id);
        $subject->name = request()->name;
        $subject->save();

        return redirect()->back();
    }

    public function deleteSubject()
    {
        $subject = Subject::find(request()->id);
        $subject->delete();

        return redirect()->back();
    }

    public function showShiftList()
    {
        $shifts = Shift::where('school_id', \Auth::user()->schoolAdmin()->first()->school->id)->orderBy('start_time')->get();
        return view('school-admin.shift', compact('shifts'));
    }

    public function addShift()
    {
        $shift = new Shift;
        $shift->school_id = \Auth::user()->schoolAdmin()->first()->school->id;
        $shift->start_time = request()->startHour . ':' . request()->startMinute;
        $shift->end_time = request()->endHour . ':' . request()->endMinute;
        $shift->save();

        return redirect()->back();
    }

    public function editShift()
    {
        $shift = Shift::find(request()->id);
        $shift->start_time = request()->startHour . ':' . request()->startMinute;
        $shift->end_time = request()->endHour . ':' . request()->endMinute;
        $shift->save();

        return redirect()->back();
    }

    public function deleteShift()
    {
        $shift = Shift::find(request()->id);
        $shift->delete();

        return redirect()->back();
    }

    public function showClassroomStudents(Classroom $classroom)
    {
        $students = $classroom->students()->with('user')
            ->orderBy('created_at', 'desc')->paginate(5);

        return view('school-admin.student-class', compact('students', 'classroom'));
    }

    public function getStudent()
    {
        $name = request()->name;
        $students = \Auth::user()->schoolAdmin()->first()->school->students()->whereHas('user', function ($query) use ($name) {
            $query->where('name', 'like', '%' . $name . '%');
        })->with('user')->orderBy('created_at', 'desc')->limit(5)->get();

        return $students;
    }

    public function addStudent()
    {
        $students = request()->students;
        $classroomId = request()->classroomId;
        foreach ($students as $student) {
            ClassroomStudent::create([
                'classroom_id' => $classroomId,
                'student_id' => $student['id']
            ]);
        }
    }

    public function showSchedulePage(Classroom $classroom)
    {
        $shifts = Shift::where('school_id', \Auth::user()->schoolAdmin()->first()->school->id)->get();
        $schedules = Schedule::whereHas('course', function ($query) use ($classroom) {
            $query->where('classroom_id', $classroom->id);
        })->get();

        return view('school-admin.schedule', compact('classroom', 'shifts', 'schedules'));
    }


    public function addCourse($classroomId, $subjectId, $teacherId)
    {
        $course = new Course;
        $course->classroom_id = $classroomId;
        $course->subject_id = $subjectId;
        $course->teacher_id = $teacherId;
        $course->save();
    }

    public function isCourseExist($courses, $subjectId, $teacherId)
    {
        foreach ($courses as $course) {
            if ($course->subject_id == $subjectId && $course->teacher_id == $teacherId) {
                return true;
            }
        }
        return false;
    }

    public function addSchedule()
    {
        $classroomId = request()->classroomId;
        $subjectId = request()->subjectId;
        $teacherId = request()->teacherId;

        $classroomCourses = Course::where('classroom_id', $classroomId)->get();

        if (!$this->isCourseExist($classroomCourses, $subjectId, $teacherId)) {
            $this->addCourse($classroomId, $subjectId, $teacherId);
            $courseId = Course::max('id');
        } else {
            $courseId = Course::where([
                'classroom_id' => $classroomId,
                'subject_id' => $subjectId,
                'teacher_id' => $teacherId
            ])->first()->id;
        }

        $schedule = new Schedule;
        $schedule->course_id = $courseId;
        $schedule->shift_id = request()->shiftId;
        $schedule->day_of_week = request()->dayOfWeek;
        $schedule->save();

        return redirect()->back();
    }

    public function editSchedule()
    {
        $classroomId = request()->classroomId;
        $subjectId = request()->subjectId;
        $teacherId = request()->teacherId;

        $classroomCourses = Course::where('classroom_id', $classroomId)->get();

        if (!$this->isCourseExist($classroomCourses, $subjectId, $teacherId)) {
            $this->addCourse($classroomId, $subjectId, $teacherId);
            $courseId = Course::max('id');
        } else {
            $courseId = Course::where([
                'classroom_id' => $classroomId,
                'subject_id' => $subjectId,
                'teacher_id' => $teacherId
            ])->first()->id;
        }

        $schedule = Schedule::find(request()->scheduleId);
        $schedule->course_id = $courseId;
        $schedule->shift_id = request()->shiftId;
        $schedule->day_of_week = request()->dayOfWeek;
        $schedule->save();

        return redirect()->back();
    }

    public function deleteSchedule()
    {
        $schedule = Schedule::find(request()->scheduleId);
        $schedule->delete();

        return redirect()->back();
    }
}
