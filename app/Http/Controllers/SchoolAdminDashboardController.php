<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SchoolAdminDashboardController extends Controller
{
    //
    public function showSchoolAdminDashboard()
    {
        return view('schooladmin.index');
    }

}
