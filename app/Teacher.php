<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function user()
    {
        return $this->morphToMany(User::class, 'role');
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    protected $guarded = [];
}
