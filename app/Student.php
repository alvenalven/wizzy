<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function user()
    {
        return $this->morphToMany(User::class, 'role');
    }

    public function assignmentSubmissions()
    {
        return $this->hasMany(AssignmentSubmission::class);
    }

    public function classrooms()
    {
        return $this->hasMany(Classroom::class);
    }

    public function classroomStudent()
    {
        return $this->hasMany(ClassroomStudent::class);
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    public function badges()
    {
        return $this->belongsToMany(Badge::class, 'student_badges');
    }

    protected $guarded = [];
}
