<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'domain' => 'wizzy.local'
    ],
    function () {
        Route::get(
            'register/{invitation}',
            [
                'as'         => 'register.index',
                'uses'       => 'Auth\\RegisterController@showRegisterForm',
                'middleware' => 'guest'
            ]
        );

        /*
         * Get Student Data
         */
        Route::get('nisn', 'NisnController@show');
        Route::post('nisn', 'NisnController@get');
    }
);

Route::group(
    [
        'domain' => 'admin.wizzy.local'
    ],
    function () {
        // School Admin Routes here

        Route::post(
            'teacher/remote',
            [
                'as'         => 'teacher.remote',
                'uses'       => 'TeacherController@getTeachersFromRemote',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'teacher',
            [
                'as'         => 'schoolAdmin.teacher',
                'uses'       => 'SchoolAdminController@showTeachersPage',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'teacher/active',
            [
                'as'         => 'schoolAdmin.teacher.active',
                'uses'       => 'SchoolAdminController@showActiveTeachersPage',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'teacher/pending',
            [
                'as'         => 'schoolAdmin.teacher.pending',
                'uses'       => 'SchoolAdminController@showPendingTeachersPage',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'teacher/invite',
            [
                'as'         => 'teacher.invite',
                'uses'       => 'SchoolAdminController@submitTeacherInvitation',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'student/remote',
            [
                'as'         => 'student.remote',
                'uses'       => 'StudentController@getStudentsFromRemote',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'student',
            [
                'as'         => 'schoolAdmin.student',
                'uses'       => 'SchoolAdminController@showStudentsPage',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'student/active',
            [
                'as'         => 'schoolAdmin.student.active',
                'uses'       => 'SchoolAdminController@showActiveStudentsPage',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'student/pending',
            [
                'as'         => 'schoolAdmin.student.pending',
                'uses'       => 'SchoolAdminController@showPendingStudentsPage',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'student/invite',
            [
                'as'         => 'student.invite',
                'uses'       => 'SchoolAdminController@submitStudentInvitation',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'teacher/resend',
            [
                'as'         => 'teacher.resend',
                'uses'       => 'SchoolAdminController@resendTeacherInvitation',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'teacher/cancel',
            [
                'as'         => 'teacher.cancel',
                'uses'       => 'SchoolAdminController@cancelTeacherInvitation',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'teacher/grant',
            [
                'as'         => 'teacher.grant',
                'uses'       => 'SchoolAdminController@grantSchoolAdminRole',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'student/resend',
            [
                'as'         => 'student.resend',
                'uses'       => 'SchoolAdminController@resendStudentInvitation',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'student/cancel',
            [
                'as'         => 'student.cancel',
                'uses'       => 'SchoolAdminController@cancelStudentInvitation',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            '',
            [
                'as'         => 'admin.index',
                'uses'       => 'AdminController@showPendingSchools',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'school/update',
            [
                'as'         => 'schoolAdmin.showUpdateSchool',
                'uses'       => 'SchoolAdminController@showUpdateSchoolForm',
                'middleware' => 'admin'
            ]
        );

        Route::patch(
            'school/update',
            [
                'as'         => 'schoolAdmin.updateSchool',
                'uses'       => 'SchoolAdminController@updateSchool',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'class',
            [
                'as'         => 'schoolAdmin.class',
                'uses'       => 'SchoolAdminController@showClassList',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'class',
            [
                'as'         => 'schoolAdmin.addClass',
                'uses'       => 'SchoolAdminController@addClassroom',
                'middleware' => 'admin'
            ]
        );

        Route::patch(
            'class',
            [
                'as'         => 'schoolAdmin.editClass',
                'uses'       => 'SchoolAdminController@editClassroom',
                'middleware' => 'admin'
            ]
        );

        Route::delete(
            'class',
            [
                'as'         => 'schoolAdmin.deleteClass',
                'uses'       => 'SchoolAdminController@deleteClassroom',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'subject',
            [
                'as'         => 'schoolAdmin.subject',
                'uses'       => 'SchoolAdminController@showSubjectList',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'subject',
            [
                'as'         => 'schoolAdmin.addSubject',
                'uses'       => 'SchoolAdminController@addSubject',
                'middleware' => 'admin'
            ]
        );

        Route::patch(
            'subject',
            [
                'as'         => 'schoolAdmin.editSubject',
                'uses'       => 'SchoolAdminController@editSubject',
                'middleware' => 'admin'
            ]
        );

        Route::delete(
            'subject',
            [
                'as'         => 'schoolAdmin.deleteSubject',
                'uses'       => 'SchoolAdminController@deleteSubject',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'shift',
            [
                'as'         => 'schoolAdmin.shift',
                'uses'       => 'SchoolAdminController@showShiftList',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'shift',
            [
                'as'         => 'schoolAdmin.addShift',
                'uses'       => 'SchoolAdminController@addShift',
                'middleware' => 'admin'
            ]
        );

        Route::patch(
            'shift',
            [
                'as'         => 'schoolAdmin.editShift',
                'uses'       => 'SchoolAdminController@editShift',
                'middleware' => 'admin'
            ]
        );

        Route::delete(
            'shift',
            [
                'as'         => 'schoolAdmin.deleteShift',
                'uses'       => 'SchoolAdminController@deleteShift',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'class/{classroom}',
            [
                'as'         => 'schoolAdmin.classroom',
                'uses'       => 'SchoolAdminController@showClassroomStudents',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'student/search',
            [
                'as'         => 'schoolAdmin.studentSearch',
                'uses'       => 'SchoolAdminController@getStudent',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'student/add',
            [
                'as'         => 'schoolAdmin.studentAdd',
                'uses'       => 'SchoolAdminController@addStudent',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'schedule/{classroom}',
            [
                'as'         => 'schoolAdmin.schedule',
                'uses'       => 'SchoolAdminController@showSchedulePage',
                'middleware' => 'admin'
            ]
        );

        Route::post(
            'schedule/add',
            [
                'as'         => 'schoolAdmin.scheduleAdd',
                'uses'       => 'SchoolAdminController@addSchedule',
                'middleware' => 'admin'
            ]
        );

        Route::patch(
            'schedule/edit',
            [
                'as'         => 'schoolAdmin.scheduleEdit',
                'uses'       => 'SchoolAdminController@editSchedule',
                'middleware' => 'admin'
            ]
        );

        Route::delete(
            'schedule/delete',
            [
                'as'         => 'schoolAdmin.scheduleDelete',
                'uses'       => 'SchoolAdminController@deleteSchedule',
                'middleware' => 'admin'
            ]
        );


        // Admin Routes here

        Route::patch(
            'approve/{school}',
            [
                'as'         => 'admin.approve',
                'uses'       => 'AdminController@approve',
                'middleware' => 'admin'
            ]
        );

        Route::delete(
            'disapprove/{school}',
            [
                'as'         => 'admin.disapprove',
                'uses'       => 'AdminController@disapprove',
                'middleware' => 'admin'
            ]
        );

        Route::get(
            'school',
            [
                'as'         => 'admin.school',
                'uses'       => 'AdminController@showApprovedSchools',
                'middleware' => 'admin'
            ]
        );

        // Auth Routes here

        Route::get(
            'login',
            [
                'as'         => 'login.index',
                'uses'       => 'Auth\\LoginController@showAdminLoginForm',
                'middleware' => 'guest'
            ]
        );

        Route::post(
            'login',
            [
                'as'         => 'login.auth',
                'uses'       => 'Auth\\LoginController@authenticate',
                'middleware' => 'guest'
            ]
        );

        Route::post(
            'logout',
            [
                'as'         => 'logout.auth',
                'uses'       => 'Auth\\LoginController@adminLogout',
                'middleware' => 'admin'
            ]
        );
    }
);

Route::group(
    [
        'domain' => 'teacher.wizzy.local'
    ],
    function () {
        // Teacher Routes here

        Route::get(
            '/',
            [
                'as'         => 'teacher.index',
                'uses'       => 'TeacherController@showTeacherDashboard',
                'middleware' => 'teacher'
            ]
        );

        Route::get(
            'schedule',
            [
                'as'         => 'teacher.schedule',
                'uses'       => 'TeacherController@showSchedulePage',
                'middleware' => 'teacher'
            ]
        );

        Route::get(
            'course/{course}/student',
            [
                'as'         => 'teacher.showStudent',
                'uses'       => 'TeacherController@showStudentsPage',
                'middleware' => 'teacher'
            ]
        );

        Route::get(
            'course/{course}/assignment',
            [
                'as'         => 'teacher.showAssignment',
                'uses'       => 'TeacherController@showAssignmentPage',
                'middleware' => 'teacher'
            ]
        );

        Route::post(
            'assignment',
            [
                'as'         => 'teacher.uploadAssignment',
                'uses'       => 'AssignmentController@addAssignment',
                'middleware' => 'teacher'
            ]
        );

        Route::patch(
            'assignment',
            [
                'as'         => 'teacher.editAssignment',
                'uses'       => 'AssignmentController@editAssignment',
                'middleware' => 'teacher'
            ]
        );

        Route::delete(
            'assignment',
            [
                'as'         => 'teacher.deleteAssignment',
                'uses'       => 'AssignmentController@deleteAssignment',
                'middleware' => 'teacher'
            ]
        );

        Route::post(
            'assignment/download',
            [
                'as'         => 'teacher.downloadAssignment',
                'uses'       => 'AssignmentController@downloadAssignment',
                'middleware' => 'teacher'
            ]
        );

        Route::post(
            'assignment/submission/download',
            [
                'as'         => 'teacher.downloadAssignmentSubmission',
                'uses'       => 'AssignmentController@downloadAssignmentSubmission',
                'middleware' => 'teacher'
            ]
        );

        Route::get(
            'assignment/{assignment}/submissions',
            [
                'as'         => 'teacher.showAssignmentSubmissions',
                'uses'       => 'TeacherController@showSubmissions',
                'middleware' => 'teacher'
            ]
        );

        Route::get(
            'course/{course}/material',
            [
                'as'         => 'teacher.showMaterial',
                'uses'       => 'TeacherController@showMaterialPage',
                'middleware' => 'teacher'
            ]
        );

        Route::post(
            'material',
            [
                'as'         => 'teacher.addMaterial',
                'uses'       => 'MaterialController@addMaterial',
                'middleware' => 'teacher'
            ]
        );

        Route::delete(
            'material',
            [
                'as'         => 'teacher.deleteMaterial',
                'uses'       => 'MaterialController@deleteMaterial',
                'middleware' => 'teacher'
            ]
        );

        Route::post(
            'material/download',
            [
                'as'         => 'teacher.downloadMaterial',
                'uses'       => 'MaterialController@downloadMaterial'
            ]
        );

        Route::get(
            'course/{course}/grade',
            [
                'as'         => 'teacher.showGradeInput',
                'uses'       => 'TeacherController@showGradeInput',
                'middleware' => 'teacher'
            ]
        );

        Route::post(
            'grade',
            [
                'as'         => 'teacher.inputGrade',
                'uses'       => 'TeacherController@inputGrade',
                'middleware' => 'teacher'
            ]
        );

        // Auth Routes here

        Route::get(
            'login',
            [
                'as'         => 'teacher.login.index',
                'uses'       => 'Auth\\LoginController@showTeacherLoginForm',
                'middleware' => 'guest'
            ]
        );

        Route::post(
            'login',
            [
                'as'         => 'teacher.login.auth',
                'uses'       => 'Auth\\LoginController@authenticateTeacher',
                'middleware' => 'guest'
            ]
        );

        Route::post(
            'logout',
            [
                'as'         => 'teacher.logout.auth',
                'uses'       => 'Auth\\LoginController@teacherLogout',
                'middleware' => 'teacher'
            ]
        );
    }
);

Route::group(
    [
        'domain' => 'student.wizzy.local'
    ],
    function () {
        // Student Routes here

        Route::get(
            '/',
            [
                'as'         => 'student.index',
                'uses'       => 'StudentController@showStudentDashboard',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'schedule',
            [
                'as'         => 'student.schedule',
                'uses'       => 'StudentController@showSchedulePage',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'course/{course}/material',
            [
                'as'         => 'student.showMaterial',
                'uses'       => 'StudentController@showMaterialPage',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'course/{course}/assignment',
            [
                'as'         => 'student.showAssignment',
                'uses'       => 'StudentController@showAssignmentPage',
                'middleware' => 'student'
            ]
        );

        Route::post(
            'assignment',
            [
                'as'         => 'student.submitAssignment',
                'uses'       => 'AssignmentController@submitAssignment',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'log/xp',
            [
                'as'         => 'student.showXpLog',
                'uses'       => 'StudentController@showXpLog',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'log/gold',
            [
                'as'         => 'student.showGoldLog',
                'uses'       => 'StudentController@showGoldLog',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'log/badge',
            [
                'as'         => 'student.showBadgeLog',
                'uses'       => 'StudentController@showBadgeLog',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'shop',
            [
                'as'         => 'student.showShop',
                'uses'       => 'StudentController@showShop',
                'middleware' => 'student'
            ]
        );

        Route::post(
            'shop',
            [
                'as'         => 'student.buyShopItem',
                'uses'       => 'StudentController@buyShopItem',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'theme',
            [
                'as'         => 'student.showTheme',
                'uses'       => 'StudentController@showTheme',
                'middleware' => 'student'
            ]
        );

        Route::post(
            'theme',
            [
                'as'         => 'student.applyTheme',
                'uses'       => 'StudentController@applyTheme',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'profile',
            [
                'as'         => 'student.showProfile',
                'uses'       => 'StudentController@showProfile',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'profile/edit',
            [
                'as'         => 'student.showEditProfile',
                'uses'       => 'StudentController@showEditProfile',
                'middleware' => 'student'
            ]
        );

        Route::get(
            'grade',
            [
                'as'         => 'student.showGrade',
                'uses'       => 'StudentController@showGrade',
                'middleware' => 'student'
            ]
        );

        Route::post(
            'profile/edit',
            [
                'as'         => 'student.editProfile',
                'uses'       => 'StudentController@editProfile',
                'middleware' => 'student'
            ]
        );

        Route::post(
            'assignment/download',
            [
                'as'         => 'student.downloadAssignment',
                'uses'       => 'AssignmentController@downloadAssignment',
                'middleware' => 'student'
            ]
        );

        // Auth Routes here

        Route::get(
            'login',
            [
                'as'         => 'student.login.index',
                'uses'       => 'Auth\\LoginController@showStudentLoginForm',
                'middleware' => 'guest'
            ]
        );

        Route::post(
            'login',
            [
                'as'         => 'student.login.auth',
                'uses'       => 'Auth\\LoginController@authenticateStudent',
                'middleware' => 'guest'
            ]
        );
    }
);

Route::patch(
    'register/{invitation}',
    [
        'as'         => 'register.update',
        'uses'       => 'Auth\\RegisterController@completeUserRegistration',
        'middleware' => 'guest'
    ]
);

Route::get('logout', function() {
    Auth::logout();
    return back();
});


Route::get('/', function () {
    return view('welcome');
});



/*
 * Get School Data
 */
Route::get('npsn', 'NpsnController@show');
Route::post('npsn', 'NpsnController@get');



Route::post(
    'school',
    [
        'as'   => 'school.index',
        'uses' => 'SchoolController@store'
    ]
);

// Get School
Route::get(
    'school/{school}',
    [
        'as'   => 'school.show',
        'uses' => 'SchoolController@show'
    ]
);

// Check for School Status
Route::post(
    'school/{npsn}',
    [
        'as'   => 'school.status',
        'uses' => 'SchoolController@getSchoolStatus'
    ]
);

Route::get(
    '',
    [
        'as'   => 'home.index',
        'uses' => function() {
            return view('home.index');
        },
        'middleware' => 'guest'
    ]
);

Route::post(
    'register/search',
    [
        'as'   => 'register.search',
        'uses' => 'NpsnController@getList'
    ]
);

Route::post(
    'register/search/{npsn}',
    [
        'as'   => 'register.get',
        'uses' => 'NpsnController@get'
    ]
);