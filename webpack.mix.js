let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.stylus('resources/assets/stylus/app.styl', 'public/css/app.css')
    .options({
        processCssUrls: false
    })
    .styles([
        // vendor styles
        'resources/assets/vendors/css/normalize.css',
        'node_modules/simplebar/dist/simplebar.css',
        // app styles
        'public/css/app.css'
    ], 'public/css/app.css')




    // winter mist
    .stylus('resources/assets/stylus/all/winter-mist.styl', 'public/css/winter-mist.css')
    .options({
        processCssUrls: false
    })
    .styles([
        // vendor styles
        'resources/assets/vendors/css/normalize.css',
        'node_modules/simplebar/dist/simplebar.css',
        // app styles
        'public/css/winter-mist.css'
    ], 'public/css/winter-mist.css')



    // cotton candy
    .stylus('resources/assets/stylus/all/cotton-candy.styl', 'public/css/cotton-candy.css')
    .options({
        processCssUrls: false
    })
    .styles([
        // vendor styles
        'resources/assets/vendors/css/normalize.css',
        'node_modules/simplebar/dist/simplebar.css',
        // app styles
        'public/css/cotton-candy.css'
    ], 'public/css/cotton-candy.css')



    // tropical-punch
    .stylus('resources/assets/stylus/all/tropical-punch.styl', 'public/css/tropical-punch.css')
    .options({
        processCssUrls: false
    })
    .styles([
        // vendor styles
        'resources/assets/vendors/css/normalize.css',
        'node_modules/simplebar/dist/simplebar.css',
        // app styles
        'public/css/tropical-punch.css'
    ], 'public/css/tropical-punch.css')













    // core script
    .js('resources/assets/js/app.js', 'public/js/app.js')
    // individual scripts
    .js('resources/assets/js/guest/pages/home/index.js', 'public/js/home.js')
    .js('resources/assets/js/guest/pages/admin/index.js', 'public/js/admin.js')
    .js('resources/assets/js/school-admin/pages/teacher/index.js', 'public/js/school-admin/teacher.js')
    .js('resources/assets/js/school-admin/pages/student/index.js', 'public/js/school-admin/student.js')
    .js('resources/assets/js/school-admin/pages/class/index.js', 'public/js/school-admin/class.js')
    .js('resources/assets/js/school-admin/pages/subject/index.js', 'public/js/school-admin/subject.js')
    .js('resources/assets/js/school-admin/pages/shift/index.js', 'public/js/school-admin/shift.js')
    .js('resources/assets/js/school-admin/pages/student-class/index.js', 'public/js/school-admin/student-class.js')
    .js('resources/assets/js/school-admin/pages/schedule/index.js', 'public/js/school-admin/schedule.js')
    .js('resources/assets/js/teacher/student/index.js', 'public/js/teacher/student.js')
    .js('resources/assets/js/teacher/assignment/index.js', 'public/js/teacher/assignment.js')
    .js('resources/assets/js/teacher/material/index.js', 'public/js/teacher/material.js')



    ;