<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('school_id');
            $table->string('nisn')->nullable()->unique();
            $table->string('name');
            $table->integer('level')->default('0');
            $table->integer('exp')->default('0');
            $table->integer('gold')->default('0');
            $table->string('color_scheme')->default('wizzy');
            $table->string('gender');
            $table->string('born_place');
            $table->date('born_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
