@extends('layout.student.master')

@section('title')
    Materi Pelajaran
@endsection

@section('content')
    <div class="dashboard__common">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/book.svg#book') }}"></use>
            </svg>
            <h1>Materi Pelajaran</h1>
        </div>
        <select class="select dashboard__course__selector" onchange="handleCourseChange(value)">
            @foreach($courses as $singleCourse)
                <option value="{{ $singleCourse->id }}"{{ $singleCourse->id == $course->id ? ' selected' : '' }}>{{ $singleCourse->subject->name }} {{ $singleCourse->classroom->name }}</option>
            @endforeach
        </select>
        @if (count($materials) > 0)
            <table class="table teacher__material__table mart--40">
                <thead>
                <tr>
                    <th></th>
                    <th>Nama Materi</th>
                    <th>Tanggal Upload</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($materials as $material)
                    <tr>
                        <td>
                            @if (pathinfo($material->file)['extension'] == 'doc'
                                || pathinfo($material->file)['extension'] == 'docx'
                                || pathinfo($material->file)['extension'] == 'docm'
                                || pathinfo($material->file)['extension'] == 'docb')
                                <svg width="30" height="30">
                                    <use xlink:href="{{ asset('images/icons/doc.svg#doc') }}"></use>
                                </svg>
                            @elseif (pathinfo($material->file)['extension'] == 'pdf')
                                <svg width="30" height="30">
                                    <use xlink:href="{{ asset('images/icons/pdf.svg#pdf') }}"></use>
                                </svg>
                            @elseif (pathinfo($material->file)['extension'] == 'ppt'
                                || pathinfo($material->file)['extension'] == 'pptx'
                                || pathinfo($material->file)['extension'] == 'pptm'
                                || pathinfo($material->file)['extension'] == 'pps'
                                || pathinfo($material->file)['extension'] == 'ppsx'
                                || pathinfo($material->file)['extension'] == 'ppsm')
                                <svg width="30" height="30">
                                    <use xlink:href="{{ asset('images/icons/ppt.svg#ppt') }}"></use>
                                </svg>
                            @endif
                        </td>
                        <td>{{ $material->name }}</td>
                        <td>{{ $material->created_at->formatLocalized('%d %B %Y') }}</td>
                        <td>
                            <form action="{{ route('teacher.downloadMaterial') }}" method="POST">
                                @csrf
                                <input type="hidden" name="materialId" value="{{ $material->id }}">
                                <button type="submit" class="teacher__action__button">
                                    <svg width="17" height="17">
                                        <use xlink:href="{{ asset('images/icons/download.svg#download') }}"></use>
                                    </svg>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="dashboard__empty__data">Belum ada materi.</div>
        @endif
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        function handleCourseChange(item) {
            var url = '{{ route('student.showMaterial', ':id') }}';
            url = url.replace(':id', item);
            window.location.href = url;
        }
    </script>
@endsection
