@extends('layout.student.master')

@section('title')
    Tugas Pelajaran
@endsection

@section('content')
    <div class="dashboard__common" id="student-assignment">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/paper.svg#paper') }}"></use>
            </svg>
            <h1>Tugas Pelajaran</h1>
        </div>
        <select class="select dashboard__course__selector" onchange="handleCourseChange(value)">
            @foreach($courses as $singleCourse)
                <option value="{{ $singleCourse->id }}"{{ $singleCourse->id == $course->id ? ' selected' : '' }}>{{ $singleCourse->subject->name }} {{ $singleCourse->classroom->name }}</option>
            @endforeach
        </select>
        @if (count($assignments) > 0)
            <table class="table teacher__assignment__table mart--40">
                <thead>
                <tr>
                    <th>Nama Tugas</th>
                    <th>Mulai</th>
                    <th>Deadline</th>
                    <th>Status Upload</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($assignments as $assignment)
                    <tr>
                        <td>{{ $assignment->name }}</td>
                        <td>{{ $assignment->start_date->formatLocalized('%d %B %Y') }}</td>
                        <td>{{ $assignment->end_date->formatLocalized('%d %B %Y') }}</td>
                        <td>
                            @if ($assignment->is_submitted)
                                <svg width="17" height="17" class="htick">
                                    <use xlink:href="{{ asset('images/icons/tick.svg#tick') }}"></use>
                                </svg>
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('student.downloadAssignment') }}" method="POST">
                                @csrf
                                <input type="hidden" name="assignmentId" value="{{ $assignment->id }}">
                                <button type="submit" class="teacher__action__button">
                                    <svg width="17" height="17">
                                        <use xlink:href="{{ asset('images/icons/download.svg#download') }}"></use>
                                    </svg>
                                </button>
                            </form>
                        </td>
                        <td>
                            @if (!$assignment->is_submitted && Carbon\Carbon::now()->lte($assignment->end_date))
                                <button class="teacher__action__button" @click="onclickUploadButton({{ $assignment->id }}, '{{ $assignment->name }}')">
                                    <svg width="17" height="17">
                                        <use xlink:href="{{ asset('images/icons/upload.svg#upload') }}"></use>
                                    </svg>
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <modal v-if="showUploadModal" @modal-close="showUploadModal = false" v-cloak>
                <div class="admin__approve">
                    <div class="admin__approve__heading">Kumpul Tugas</div>
                    <div class="admin__school__name">@{{ assignment.name }}</div>
                    <form action="{{ route('student.submitAssignment') }}" method="POST" enctype="multipart/form-data"
                          class="teacher__form">
                        @csrf
                        <div class="form__row marb--10">
                            <label for="submission-file" class="sadmin__label">Upload Tugas</label>
                            <input type="file" name="submission" id="submission-file">
                        </div>
                        <input type="hidden" name="assignmentId" value="{{ $assignment->id }}">
                        <div class="flex flex--center mart--30">
                            <button type="submit" class="button"><span>Simpan</span></button>
                        </div>
                    </form>
                </div>
            </modal>
        @else
            <div class="dashboard__empty__data">Belum ada materi.</div>
        @endif
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        function handleCourseChange(item) {
            var url = '{{ route('student.showAssignment', ':id') }}';
            url = url.replace(':id', item);
            window.location.href = url;
        }

        new Vue({
            el: '#student-assignment',
            data: {
                showUploadModal: false,
                assignment: {
                    name: '',
                    id: 0
                }
            },
            methods: {
                onclickUploadButton: function (id, name) {
                    this.assignment.id = id;
                    this.assignment.name = name;
                    this.showUploadModal = true;
                }
            }
        });
    </script>
@endsection
