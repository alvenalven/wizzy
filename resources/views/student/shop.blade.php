@extends('layout.student.master')

@section('title')
    Toko
@endsection

@section('content')
    <div class="dashboard__common">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/cart.svg#cart') }}"></use>
            </svg>
            <h1>Toko</h1>
        </div>
        <div class="sadmin__tab">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showShop' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showShop') }}">Tema</a>
            </div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showTheme' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showTheme') }}">Tema Ku</a>
            </div>
        </div>

        <table class="table dashboard__theme__table mart--40">
            <thead>
            <tr>
                <th></th>
                <th>Nama Tema</th>
                <th>Harga</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div class="dashboard__table__theme dashboard__table__theme--wm"></div>
                </td>
                <td>Winter Mist</td>
                <td>
                    <div class="dashboard__scoin__container">
                        <div class="dashboard__sccoin marr--15"></div>
                        <div class="dashboard__sccoin__table">1000 Gold</div>
                    </div>
                </td>
                @if (Auth::user()->student()->first()->gold < 1000 && count(App\StudentItem::where('item_id', 1)->get()) == 0)
                    <td class="table-cell">
                        <div class="dashboard__theme__bought text--red">Gold tidak mencukupi</div>
                    </td>
                @elseif (count(App\StudentItem::where('item_id', 1)->get()) == 0)
                    <td>
                        <form action="{{ route('student.buyShopItem') }}" method="POST">
                            @csrf
                            <input type="hidden" name="itemId" value="1">
                            <button class="button" type="submit"><span>Beli</span></button>
                        </form>
                    </td>
                @else
                    <td class="table-cell">
                        <div class="dashboard__theme__bought text--green">Sudah dibeli</div>
                    </td>
                @endif
            </tr>
            <tr>
                <td>
                    <div class="dashboard__table__theme dashboard__table__theme--tp"></div>
                </td>
                <td>Tropical Punch</td>
                <td>
                    <div class="dashboard__scoin__container">
                        <div class="dashboard__sccoin marr--15"></div>
                        <div class="dashboard__sccoin__table">1000 Gold</div>
                    </div>
                </td>
                @if (Auth::user()->student()->first()->gold < 1000 && count(App\StudentItem::where('item_id', 2)->get()) == 0)
                    <td class="table-cell">
                        <div class="dashboard__theme__bought text--red">Gold tidak mencukupi</div>
                    </td>
                @elseif (count(App\StudentItem::where('item_id', 2)->get()) == 0)
                    <td>
                        <form action="{{ route('student.buyShopItem') }}" method="POST">
                            @csrf
                            <input type="hidden" name="itemId" value="2">
                            <button class="button" type="submit"><span>Beli</span></button>
                        </form>
                    </td>
                @else
                    <td class="table-cell">
                        <div class="dashboard__theme__bought text--green">Sudah dibeli</div>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
    </div>
@endsection
