@extends('layout.student.master')

@section('title')
    Log Badge
@endsection

@section('content')
    <div class="dashboard__common" id="student-assignment">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/star.svg#star') }}"></use>
            </svg>
            <h1>Log</h1>
        </div>
        <div class="sadmin__tab sadmin__tab--3">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showXpLog' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showXpLog') }}">XP</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showGoldLog' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showGoldLog') }}">Gold</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showBadgeLog' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showBadgeLog') }}">Badge</a></div>
        </div>
        <div class="dashboard__logs">
            @foreach($badges as $badge)
                <div class="dashboard__log{{ $badge->id == 3 || $badge->id == 6 || $badge->id == 8 || $badge->id == 9 ? ' marb--40' : '' }}">
                    <div class="dashboard__log__value dashboard__log__value--img{{ $badge->has_badge ? '' : ' dashboard__log__value--na'  }}">
                        @if ($badge->id != 7 && $badge->id != 8)
                            <img src="{{ asset('images/' . $badge->image) }}" alt="">
                        @else
                            <img src="{{ asset('images/' . $badge->image) }}" class="special" alt="">
                        @endif
                    </div>
                    <div class="dashboard__log__desc{{ $badge->has_badge ? '' : ' dashboard__log__desc--na'  }}">
                        <div class="dashboard__log__udesc">
                            {{ $badge->name }}
                        </div>
                        <div class="dashboard__log__ldesc">
                            {{ $badge->desc }}
                        </div>
                    </div>
                    <div class="dashboard__log__date">
                        @if ($badge->created_at)
                            dicapai pada <span class="text--bold">{{ $badge->created_at->formatLocalized('%d %B %Y') }}</span>
                        @else
                            <div class="dashboard__progress__desc">
                                @if ($badge->id == 1)
                                    0/1
                                @elseif ($badge->id == 2)
                                    <?php $value = count(App\AssignmentSubmission::where('student_id', Auth::user()->student->first()->id)->get()) ?>
                                    {{ $value }}/5
                                @elseif ($badge->id == 3)
                                    <?php $value = count(App\AssignmentSubmission::where('student_id', Auth::user()->student->first()->id)->get()) ?>
                                    {{ $value }}/10
                                @elseif ($badge->id == 4)
                                    0/1
                                @elseif ($badge->id == 5)
                                    <?php $value = count(App\MaterialDownload::where('student_id', Auth::user()->student->first()->id)->get()) ?>
                                    {{ $value }}/5
                                @elseif ($badge->id == 6)
                                    <?php $value = count(App\MaterialDownload::where('student_id', Auth::user()->student->first()->id)->get()) ?>
                                    {{ $value }}/10
                                @elseif ($badge->id == 7)
                                    {{ $fastSubmissionCount }}/5
                                @elseif ($badge->id == 8)
                                    {{ $fastSubmissionCount }}/10
                                @elseif ($badge->id == 9)
                                    0/1
                                @elseif ($badge->id == 10)
                                    0/1
                                @endif
                            </div>
                            <div class="dashboard__lv__barc dashboard__lv__barc--log">
                                @if ($badge->id == 1)
                                    <div class="dashboard__lv__bar" style="width: 0%;"></div>
                                @elseif ($badge->id == 2)
                                    <div class="dashboard__lv__bar" style="width: {{ $value / 5 * 100 }}%;"></div>
                                @elseif ($badge->id == 3)
                                    <div class="dashboard__lv__bar" style="width: {{ $value / 10 * 100 }}%;"></div>
                                @elseif ($badge->id == 4)
                                    <div class="dashboard__lv__bar" style="width: 0%;"></div>
                                @elseif ($badge->id == 5)
                                    <div class="dashboard__lv__bar" style="width: {{ $value / 5 * 100 }}%;"></div>
                                @elseif ($badge->id == 6)
                                    <div class="dashboard__lv__bar" style="width: {{ $value / 10 * 100 }}%;"></div>
                                @elseif ($badge->id == 7)
                                    <div class="dashboard__lv__bar" style="width: {{ $fastSubmissionCount / 5 * 100 }}%;"></div>
                                @elseif ($badge->id == 8)
                                    <div class="dashboard__lv__bar" style="width: {{ $fastSubmissionCount / 10 * 100 }}%;"></div>
                                @elseif ($badge->id == 9)
                                    <div class="dashboard__lv__bar" style="width: 0%;"></div>
                                @elseif ($badge->id == 10)
                                    <div class="dashboard__lv__bar" style="width: 0%;"></div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
