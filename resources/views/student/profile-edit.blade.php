@extends('layout.student.master')

@section('title')
    Ubah Profil
@endsection

@section('content')
    <div class="dashboard__common" id="student-assignment">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/user.svg#user') }}"></use>
            </svg>
            <h1>Ubah Profil</h1>
        </div>
        <div class="sadmin__info mart--40">
            <form action="{{ route('student.editProfile') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">Foto Profil</div>
                    <div class="sadmin__itable__text">
                        <input type="file" name="photo">
                    </div>
                </div>
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">NISN</div>
                    <div class="sadmin__itable__text">
                        <input type="text" class="input" value="{{ Auth::user()->student()->first()->nisn }}" disabled>
                    </div>
                </div>
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">Tempat Lahir</div>
                    <div class="sadmin__itable__text">
                        <input type="text" class="input" value="{{ Auth::user()->born_place }}" disabled>
                    </div>
                </div>
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">Tanggal Lahir</div>
                    <div class="sadmin__itable__text">
                        <input type="text" class="input" value="{{ Auth::user()->born_date->format('Y-m-d') }}" disabled>
                    </div>
                </div>
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">Alamat</div>
                    <div class="sadmin__itable__text">
                        <input type="text" class="input" value="{{ Auth::user()->address }}" name="address">
                    </div>
                </div>
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">Username</div>
                    <div class="sadmin__itable__text">
                        <input type="text" class="input" value="{{ Auth::user()->username }}" disabled>
                    </div>
                </div>
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">Email</div>
                    <div class="sadmin__itable__text">
                        <input type="text" class="input" value="{{ Auth::user()->email }}" name="email">
                    </div>
                </div>
                <div class="sadmin__itable__data">
                    <div></div>
                    <div class="sadmin__itable__text text--bold">Telepon</div>
                    <div class="sadmin__itable__text">
                        <input type="text" class="input" value="{{ Auth::user()->phone }}" name="phone">
                    </div>
                </div>
                <div class="flex mart--30">
                    <a href="{{ route('student.showProfile') }}" class="button button--invert marr--30">
                        <span>Batalkan</span>
                    </a>
                    <button class="button" type="submit">
                        <span>
                            Simpan Perubahan
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
