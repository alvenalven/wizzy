@extends('layout.student.master')

@section('title')
    Nilai
@endsection

@section('content')
    <div class="dashboard__common" id="student-assignment">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            <h1>Nilai</h1>
        </div>
        <div class="dashboard__grade">
            <div class="dashboard__grade__header">
                <span>Nama Mata Pelajaran</span>
                <span>Komponen</span>
                <span>Nilai</span>
                <span>Rata-Rata</span>
            </div>
            @foreach($studentGrades as $studentGrade)
                <div class="dashboard__grade__item">
                    <div>{{ $studentGrade->course->subject->name }} {{ $studentGrade->course->classroom->name }}</div>
                    <div>
                        <span>Ujian Tengah Semester</span>
                        <span>Ujian Akhir</span>
                    </div>
                    <div>
                        <span>{{ $studentGrade->mid }}</span>
                        <span>{{ $studentGrade->final }}</span>
                    </div>
                    <div>{{ ( $studentGrade->mid + $studentGrade->final ) / 2 }}</div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
