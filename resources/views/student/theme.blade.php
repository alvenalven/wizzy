@extends('layout.student.master')

@section('title')
    Tema Ku
@endsection

@section('content')
    <div class="dashboard__common">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/cart.svg#cart') }}"></use>
            </svg>
            <h1>Toko</h1>
        </div>
        <div class="sadmin__tab">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showShop' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showShop') }}">Tema</a>
            </div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showTheme' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showTheme') }}">Tema Ku</a>
            </div>
        </div>

        <table class="table dashboard__theme__table mart--40">
            <thead>
            <tr>
                <th></th>
                <th>Nama Tema</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div class="dashboard__table__theme dashboard__table__theme--cc"></div>
                </td>
                <td>Cotton Candy</td>
                @if (Auth::user()->student()->first()->color_scheme != 'wizzy')
                    <td>
                        <form action="{{ route('student.applyTheme') }}" method="POST">
                            @csrf
                            <input type="hidden" name="itemId" value="0">
                            <button class="button" type="submit"><span>Terapkan</span></button>
                        </form>
                    </td>
                @else
                    <td class="table-cell">
                        <div class="dashboard__theme__bought text--green">Sedang aktif</div>
                    </td>
                @endif
            </tr>
            @foreach ($items as $index => $item)
                <tr>
                    <td>
                        <div class="dashboard__table__theme dashboard__table__theme--{{ $item->item_id == 1 ? 'wm' : '' }}{{ $item->item_id == 2 ? 'tp' : '' }}"></div>
                    </td>
                    <td>
                        @if ($item->item_id == 1)
                            Winter Mist
                        @elseif ($item->item_id == 2)
                            Tropical Punch
                        @endif
                    </td>
                    @if ($item->item_id == 1)
                        @if (Auth::user()->student()->first()->color_scheme == 'winter-mist')
                            <td class="table-cell">
                                <div class="dashboard__theme__bought text--green">Sedang aktif</div>
                            </td>
                        @else
                            <td>
                                <form action="{{ route('student.applyTheme') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="itemId" value="1">
                                    <button class="button" type="submit"><span>Terapkan</span></button>
                                </form>
                            </td>
                        @endif
                    @elseif ($item->item_id == 2)
                        @if (Auth::user()->student()->first()->color_scheme == 'tropical-punch')
                            <td class="table-cell">
                                <div class="dashboard__theme__bought text--green">Sedang aktif</div>
                            </td>
                        @else
                            <td>
                                <form action="{{ route('student.applyTheme') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="itemId" value="2">
                                    <button class="button" type="submit"><span>Terapkan</span></button>
                                </form>
                            </td>
                        @endif
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
