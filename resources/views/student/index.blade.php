@extends('layout.student.master')

@section('title')
    Beranda Siswa
@endsection

@section('content')
    <div class="dashboard__index">
        <div class="dashboard__left">
            <div class="dashboard__content__item dashboard__content__item--bigside">
                <div class="dashboard__index__header">Jadwal</div>
                @if (count($todaySchedule) == 0 && count($tomorrowSchedule) == 0)
                    <div class="dashboard__index__subheader no-margin">Tidak ada jadwal untuk hari ini dan besok.</div>
                @else
                    @if ($currentSchedule || count($todaySchedule) > 0)
                        <div class="dashboard__index__subheader">Jadwal Kamu Hari ini</div>
                    @else
                        <div class="dashboard__index__subheader">Tidak ada jadwal untuk hari ini.</div>
                    @endif
                    @if ($currentSchedule)
                        <a href="{{ route('student.showMaterial', $currentSchedule->course->id) }}" class="dashboard__schedule dashboard__schedule--active">
                            <div class="dashboard__schedule__class">{{ $currentSchedule->course->subject->name }} {{ $currentSchedule->course->classroom->name }}</div>
                            <div class="dashboard__schedule__time">{{ substr($currentSchedule->shift->start_time, 0, 5) }} - {{ substr($currentSchedule->shift->end_time, 0, 5) }}</div>
                        </a>
                    @endif
                    @foreach($todaySchedule as $schedule)
                        <a href="{{ route('student.showMaterial', $schedule->course->id) }}" class="dashboard__schedule">
                            <div class="dashboard__schedule__class">{{ $schedule->course->subject->name }} {{ $schedule->course->classroom->name }}</div>
                            <div class="dashboard__schedule__time">{{ substr($schedule->shift->start_time, 0, 5) }} - {{ substr($schedule->shift->end_time, 0, 5) }}</div>
                        </a>
                    @endforeach
                    @if (count($tomorrowSchedule) > 0)
                        <div class="dashboard__index__subheader">Jadwal Kamu Besok</div>
                        @foreach($tomorrowSchedule as $schedule)
                            <a href="{{ route('student.showMaterial', $schedule->course->id) }}" class="dashboard__schedule">
                                <div class="dashboard__schedule__class">{{ $schedule->course->subject->name }} {{ $schedule->course->classroom->name }}</div>
                                <div class="dashboard__schedule__time">{{ substr($schedule->shift->start_time, 0, 5) }} - {{ substr($schedule->shift->end_time, 0, 5) }}</div>
                            </a>
                        @endforeach
                    @endif
                @endif
            </div>
        </div>
        <div class="dashboard__right">
            <div class="dashboard__content__item">
                <div class="dashboard__index__header">Tugas yang Sedang Berjalan</div>
                @if (count($assignments) > 0)
                    @foreach($assignments as $assignment)
                        <a href="{{ route('student.showAssignment', $assignment->course->id) }}" class="dashboard__schedule dashboard__schedule--assignment">
                            <div class="dashboard__schedule__class">{{ $assignment->course->subject->name }} {{ $assignment->course->classroom->name }}</div>
                            <div class="dashboard__schedule__time flex" style="align-items: center">
                                <svg width="11" height="11" style="fill: #999; margin-right: 7px">
                                    <use xlink:href="{{ asset('images/icons/time-small.svg#time-small') }}"></use>
                                </svg>
                                {{ $assignment->end_date->formatLocalized('%d %B %Y') }}
                            </div>
                            @if ($assignment->has_submit)
                                <svg width="20" height="20" class="htick dashboard__assignment__done">
                                    <use xlink:href="{{ asset('images/icons/tick.svg#tick') }}"></use>
                                </svg>
                            @endif
                        </a>
                    @endforeach
                @else
                    Belum ada tugas.
                @endif
            </div>
            <div class="dashboard__content__item dashboard__content__item--bigside">
                <div class="dashboard__index__header">Leaderboard</div>
                <div class="dashboard__leaderboard">
                    @foreach ($classroomStudentsFinal as $classroomStudent)
                        <div class="dashboard__leaderboard__item{{ Auth::user()->student()->first()->id == $classroomStudent['id'] ? ' dashboard__leaderboard__item--self' : '' }}">
                            @if (Auth::user()->student()->first()->id == $classroomStudent['id'])
                                <span>Kamu</span>
                            @else
                                <span>{{ $classroomStudent['name'] }}</span>
                            @endif
                            <span>{{ $classroomStudent['total_point'] }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
