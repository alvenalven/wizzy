@extends('layout.student.master')

@section('title')
    Log Gold
@endsection

@section('content')
    <div class="dashboard__common" id="student-assignment">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/star.svg#star') }}"></use>
            </svg>
            <h1>Log</h1>
        </div>
        <div class="sadmin__tab sadmin__tab--3">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showXpLog' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showXpLog') }}">XP</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showGoldLog' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showGoldLog') }}">Gold</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'student.showBadgeLog' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('student.showBadgeLog') }}">Badge</a></div>
        </div>
        @if (count($logs) > 0)
            <div class="dashboard__logs">
                @foreach($logs as $log)
                    <div class="dashboard__log">
                        <div class="dashboard__log__value dashboard__log__value--{{ $log->source != 'shop' ? 'plus' : 'minus' }}">{{ $log->source != 'shop' ? '+' : '-' }}{{ $log->value }}</div>
                        <div class="dashboard__log__desc">
                            <div class="dashboard__log__udesc no-margin">
                                @if ($log->source == 'material')
                                    Mengunduh materi pelajaran {{ $log->course->subject->name }} {{ $log->course->classroom->name }}
                                @elseif ($log->source == 'assignment')
                                    Mengumpulkan tugas pelajaran {{ $log->course->subject->name }} {{ $log->course->classroom->name }}
                                @elseif ($log->source == 'shop')
                                    Membeli barang di toko
                                @elseif ($log->source == 'login')
                                    Login harian
                                @endif
                            </div>
                        </div>
                        <div class="dashboard__log__date">
                            dicapai pada <span class="text--bold">{{ $log->created_at->formatLocalized('%d %B %Y') }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="dashboard__empty__data">Belum ada data log.</div>
        @endif
    </div>
@endsection
