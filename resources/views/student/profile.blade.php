@extends('layout.student.master')

@section('title')
    Profil
@endsection

@section('content')
    <div class="dashboard__common" id="student-assignment">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/user.svg#user') }}"></use>
            </svg>
            <h1>Profil</h1>
        </div>
        <div class="sadmin__info mart--40">
            <div class="sadmin__itable__header">
                @if (Auth::user()->photo)
                    <img src="{{ asset('images/' . Auth::user()->photo) }}" class="dashboard__profile__image marb--20">
                @else
                    <div class="dashboard__profile__empty dashboard__profile__empty--student marb--20">
                        <svg width="75" height="75">
                            <use xlink:href="{{ asset('images/icons/user.svg#user') }}"></use>
                        </svg>
                    </div>
                @endif
                <a href="{{ route('student.showEditProfile') }}" class="button button--invert sadmin__info__edit">
                    <span>
                        <svg width="14" height="14">
                            <use xlink:href="{{ asset('images/icons/edit.svg#edit') }}"></use>
                        </svg>
                        Ubah Data
                    </span>
                </a>
            </div>
            <div class="sadmin__itable__data">
                <div></div>
                <div class="sadmin__itable__text text--bold">NISN</div>
                <div class="sadmin__itable__text">{{ Auth::user()->student()->first()->nisn }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div></div>
                <div class="sadmin__itable__text text--bold">Tempat Tanggal Lahir</div>
                <div class="sadmin__itable__text">{{ Auth::user()->born_place }}{{ Auth::user()->born_place && Auth::user()->born_date ? ', ' : '' }}{{ Auth::user()->born_date->formatLocalized('%d %B %Y') }}{{ !Auth::user()->born_place && !Auth::user()->born_date ? '-' : '' }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div></div>
                <div class="sadmin__itable__text text--bold">Alamat</div>
                <div class="sadmin__itable__text">{{ Auth::user()->address ? Auth::user()->address : '-' }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div></div>
                <div class="sadmin__itable__text text--bold">Username</div>
                <div class="sadmin__itable__text">{{ Auth::user()->username }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div></div>
                <div class="sadmin__itable__text text--bold">Email</div>
                <div class="sadmin__itable__text">{{ Auth::user()->email }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div></div>
                <div class="sadmin__itable__text text--bold">Telepon</div>
                <div class="sadmin__itable__text">{{ Auth::user()->phone ? Auth::user()->phone : '-' }}</div>
            </div>
        </div>
    </div>
@endsection
