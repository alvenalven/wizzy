@extends('layout.plain')

@section('content')
    <div class="logadmin">
        <div class="logadmin__illustration">WIZZY</div>
        <div class="logadmin__box">
            <h1>Aktivasi Akun</h1>
            <form action="{{ route('register.update', $code) }}" method="POST">
                @csrf
                {{ method_field('PATCH') }}
                <div class="hform__row">
                    <label class="hlabel">Nama</label>
                    <input class="hinput" type="text" value="{{ $user['name'] }}" disabled>
                </div>
                <div class="hform__row{{ $errors->has('username') ? ' hform__row--error' : '' }}{{ old('username') && !$errors->has('username') ? ' hform__row--success' : '' }}">
                    <label class="hlabel" for="username">Username</label>
                    <input class="hinput" id="username" name="username" type="text" value="{{ old('username') }}" autofocus>
                    <span class="error">{{ $errors->first('username') }}</span>
                </div>
                <div class="hform__row{{ $errors->has('password') ? ' hform__row--error' : ''}}{{ old('password') && !$errors->has('password') ? ' hform__row--success' : '' }}">
                    <label class="hlabel" for="password">Password</label>
                    <input class="hinput" id="password" name="password" type="password" value="{{ old('password') }}">
                    <span class="error">{{ $errors->first('password') }}</span>
                </div>
                <div class="hform__row{{ $errors->has('conf_password') ? ' hform__row--error' : ''}}{{ old('conf_username') && !$errors->has('conf_username') ? ' hform__row--success' : '' }}">
                    <label class="hlabel" for="password">Repeat Password</label>
                    <input class="hinput" id="conf_password" name="conf_password" type="password">
                    <span class="error">{{ $errors->first('conf_password') }}</span>
                </div>
                <div class="hform__row hform__row--center">
                    <button class="hbutton" type="submit"><span>Aktifkan</span></button>
                </div>
            </form>
        </div>
    </div>
@endsection
