<nav>
    <a href="{{ route('admin.index') }}"{{ Route::currentRouteName() == 'admin.index' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            Beranda
        </span>
    </a>
    <a href="{{ route('admin.school') }}"{{ Route::currentRouteName() == 'admin.school' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            Daftar Sekolah
        </span>
    </a>
</nav>