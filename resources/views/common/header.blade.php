<header>
    <div class="header__logo">
        <span style="color:#3d74c5;">W</span>
        <span style="color:#4267b6;">I</span>
        <span style="color:#475aa8;">Z</span>
        <span style="color:#4b4d99;">Z</span>
        <span style="color:#50408a;">Y</span>
    </div>
    <div class="header__main">
        <div class="header__search"></div>
        <div class="header__menu">
            <a href="#" class="header__menu__item">
                <svg width="27" height="27">
                    <use href="{{ asset('images/icons/settings.svg#settings') }}"></use>
                </svg>
            </a>
            <form action="{{ route('logout.auth') }}">
                @csrf
                <button class="header__menu__item">
                    <svg width="27" height="27">
                        <use href="{{ asset('images/icons/exit.svg#exit') }}"></use>
                    </svg>
                </button>
            </form>
        </div>
    </div>
</header>
