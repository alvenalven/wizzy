<nav>
    <a href="{{ route('admin.index') }}"{{ Route::currentRouteName() == 'admin.index' || Route::currentRouteName() == 'schoolAdmin.showUpdateSchool' || Route::currentRouteName() == 'schoolAdmin.class' || Route::currentRouteName() == 'schoolAdmin.subject' || Route::currentRouteName() == 'schoolAdmin.shift' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            Beranda
        </span>
    </a>
    <a href="{{ route('admin.school') }}"{{ Route::currentRouteName() == 'admin.school' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/key.svg#key') }}"></use>
            </svg>
            Admin
        </span>
    </a>
    <a href="{{ route('schoolAdmin.teacher') }}"{{ Route::currentRouteName() == 'schoolAdmin.teacher' || Route::currentRouteName() == 'schoolAdmin.teacher.active' || Route::currentRouteName() == 'schoolAdmin.teacher.pending' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/glasses.svg#glasses') }}"></use>
            </svg>
            Guru
        </span>
    </a>
    <a href="{{ route('schoolAdmin.student') }}"{{ Route::currentRouteName() == 'schoolAdmin.student' || Route::currentRouteName() == 'schoolAdmin.student.active' || Route::currentRouteName() == 'schoolAdmin.student.pending' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/people.svg#people') }}"></use>
            </svg>
            Siswa
        </span>
    </a>
    <a href="#" class="no-action{{ Route::currentRouteName() == 'schoolAdmin.classroom' ? ' active-primary' : '' }}">
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/door.svg#door') }}"></use>
            </svg>
            Kelas
        </span>
    </a>
    @foreach (Auth::user()->schoolAdmin()->first()->school->classrooms as $classroom)
        <a class="nav__secondary{{ $classroom->id == basename(Request::path()) ? ' active' : '' }}" href="{{ route('schoolAdmin.classroom', $classroom->id) }}">{{ $classroom->name }}</a>
    @endforeach
</nav>
