<div class="dashboard__student__profile">
    <div class="dashboard__profile__sleft">
        @if (Auth::user()->photo)
            <img src="{{ asset('images/' . Auth::user()->photo) }}" class="dashboard__profile__image no-margin">
        @else
            <div class="dashboard__profile__empty dashboard__profile__empty--student no-margin">
                <svg width="75" height="75">
                    <use xlink:href="{{ asset('images/icons/user.svg#user') }}"></use>
                </svg>
            </div>
        @endif
    </div>
    <div class="dashboard__profile__scenter">
        <div class="dashboard__scname mart--10">
            {{ Auth::user()->name }}
        </div>
        <div class="dashboard__scnim">
            {{ Auth::user()->student->first()->nisn }}
        </div>
        <div class="dashboard__scclass">
            {{ count(\App\ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->get()) >= 1 ? \App\ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->first()->classroom->name : 'Belum ada kelas' }}
        </div>
    </div>
    <div class="dashboard__profile__sright">
        <div class="dashboard__scoin__container mart--10">
            <div class="dashboard__sccoin"></div>
            <div class="dashboard__sccoint">{{ Auth::user()->student->first()->gold }}</div>
        </div>
        <div class="dashboard__lv">
            <div class="dashboard__lv__txt">
                <span>LEVEL {{ Auth::user()->student->first()->level }}</span>
                <span>{{ Auth::user()->student->first()->exp }}/{{ Auth::user()->student->first()->level * 100 }}</span>
            </div>
            <div class="dashboard__lv__barc">
                <div class="dashboard__lv__bar" style="width: {{ Auth::user()->student->first()->exp % 100 }}%;"></div>
            </div>
        </div>
    </div>
</div>
