<header>
    <div class="header__logo">
        @if (Auth::user()->student()->first()->color_scheme == 'wizzy')
            <span style="color:#d79aa7;">W</span>
            <span style="color:#c3a9b4;">I</span>
            <span style="color:#aeb8c1;">Z</span>
            <span style="color:#9ac6cd;">Z</span>
            <span style="color:#85d5da;">Y</span>
        @elseif (Auth::user()->student()->first()->color_scheme == 'winter-mist')
            <span style="color:#3d74c5;">W</span>
            <span style="color:#4267b6;">I</span>
            <span style="color:#475aa8;">Z</span>
            <span style="color:#4b4d99;">Z</span>
            <span style="color:#50408a;">Y</span>
        @elseif (Auth::user()->student()->first()->color_scheme == 'tropical-punch')
            <span style="color:#ed549c;">W</span>
            <span style="color:#e5678b;">I</span>
            <span style="color:#dc7a79;">Z</span>
            <span style="color:#d48c68;">Z</span>
            <span style="color:#cb9f56;">Y</span>
        @endif
    </div>
    <div class="header__navigation">
        <div class="header__navigation__section">
            @include('common.student.navigation')
        </div>
        <div class="header__option__section">
            <form action="{{ route('logout.auth') }}">
                @csrf
                <button class="header__menu__item" title="Logout">
                    <svg width="27" height="27">
                        <use href="{{ asset('images/icons/exit.svg#exit') }}"></use>
                    </svg>
                </button>
            </form>
        </div>
    </div>
</header>
