<div class="dashboard__student__profile dashboard__student__profile--index">
    <div class="dashboard__profile__sleft">
        @if (Auth::user()->photo)
            <img src="{{ asset('images/' . Auth::user()->photo) }}" class="dashboard__profile__image">
        @else
            <div class="dashboard__profile__empty dashboard__profile__empty--student">
                <svg width="75" height="75">
                    <use xlink:href="{{ asset('images/icons/user.svg#user') }}"></use>
                </svg>
            </div>
        @endif
        <div class="dashboard__spbelow">
            {{ App\Point::where('student_id', Auth::user()->student()->first()->id)->sum('value') }} <span>Poin</span>
        </div>
        <div class="dashboard__spbelow no-margin">
            {{ count(\App\ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->get()) >= 1 ? \App\ClassroomStudent::where('student_id', \Auth::user()->student->first()->id)->first()->classroom->name : 'Belum ada kelas' }}
        </div>
    </div>
    <div class="dashboard__profile__scenter">
        <div class="dashboard__scname">
            {{ Auth::user()->name }}
        </div>
        <div class="dashboard__scnim">
            {{ Auth::user()->student->first()->nisn }}
        </div>
        <div class="dashboard__scoin__container">
            <div class="dashboard__sccoin"></div>
            <div class="dashboard__sccoint">{{ Auth::user()->student->first()->gold }}</div>
        </div>
        <div class="dashboard__lv dashboard__lv--index">
            <div class="dashboard__lv__txt">
                <span>LEVEL {{ Auth::user()->student->first()->level }}</span>
                <span>{{ Auth::user()->student->first()->exp }}/{{ Auth::user()->student->first()->level * 100 }}</span>
            </div>
            <div class="dashboard__lv__barc">
                <div class="dashboard__lv__bar" style="width: {{ Auth::user()->student->first()->exp % 100 }}%;"></div>
            </div>
        </div>
    </div>
    <div class="dashboard__profile__sright dashboard__profile__sright--index">
        <a href="{{ route('student.showBadgeLog') }}" class="dashboard__badges__heading">Badges</a>
        <div class="dashboard__badges">
            @if (count($badges) > 0)
                @foreach($badges as $badge)
                    <img src="{{ asset('images/' . $badge->image) }}" alt="">
                @endforeach
            @else
                Kamu belum memiliki badge
            @endif
        </div>
    </div>
</div>
