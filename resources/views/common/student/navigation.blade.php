<a href="{{ route('student.index') }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.index' ? ' header__navigation__item--active' : '' }}" title="Beranda">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
    </svg>
</a>
<a href="{{ route('student.schedule') }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.schedule' ? ' header__navigation__item--active' : '' }}" title="Jadwal">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/time.svg#time') }}"></use>
    </svg>
</a>
<a href="{{ route('student.showMaterial', Auth::user()->student()->first()->classroomStudent()->first()->classroom->courses()->first()->id) }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.showMaterial' ? ' header__navigation__item--active' : '' }}" title="Materi Pelajaran">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/book.svg#book') }}"></use>
    </svg>
</a>
<a href="{{ route('student.showAssignment', Auth::user()->student()->first()->classroomStudent()->first()->classroom->courses()->first()->id) }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.showAssignment' ? ' header__navigation__item--active' : '' }}" title="Tugas">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/paper.svg#paper') }}"></use>
    </svg>
</a>
<a href="{{ route('student.showGrade') }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.showGrade' ? ' header__navigation__item--active' : '' }}" title="Nilai">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
    </svg>
</a>
<a href="{{ route('student.showXpLog') }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.showXpLog' || Route::currentRouteName() == 'student.showGoldLog' || Route::currentRouteName() == 'student.showBadgeLog' ? ' header__navigation__item--active' : '' }}" title="Log">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/star.svg#star') }}"></use>
    </svg>
</a>
<a href="{{ route('student.showShop') }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.showShop' || Route::currentRouteName() == 'student.showTheme' ? ' header__navigation__item--active' : '' }}" title="Toko">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/cart.svg#cart') }}"></use>
    </svg>
</a>
<a href="{{ route('student.showProfile') }}" class="header__navigation__item{{ Route::currentRouteName() == 'student.showProfile' || Route::currentRouteName() == 'student.showEditProfile' ? ' header__navigation__item--active' : '' }}" title="Profil">
    <svg width="24" height="24">
        <use xlink:href="{{ asset('images/icons/user.svg#user') }}"></use>
    </svg>
</a>
