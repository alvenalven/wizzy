<nav>
    <a href="{{ route('teacher.index') }}"{{ Route::currentRouteName() == 'teacher.index' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            Beranda
        </span>
    </a>
    <a href="{{ route('teacher.schedule') }}"{{ Route::currentRouteName() == 'teacher.schedule' ? 'class=active' : '' }}>
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/time.svg#time') }}"></use>
            </svg>
            Jadwal
        </span>
    </a>
    <a href="#" class="no-action{{ Route::currentRouteName() == 'teacher.showStudent' || Route::currentRouteName() == 'teacher.showAssignment' || Route::currentRouteName() == 'teacher.showMaterial' ? ' active-primary' : '' }}">
        <span>
            <svg width="17.5" height="17.5">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            Pelajaran
        </span>
    </a>
    @foreach (Auth::user()->teacher()->first()->courses as $course)
        <a class="nav__secondary nav__secondary--sub{{ (Route::currentRouteName() == 'teacher.showStudent' || Route::currentRouteName() == 'teacher.showAssignment' || Route::currentRouteName() == 'teacher.showGradeInput' || Route::currentRouteName() == 'teacher.showMaterial') && $course->id == explode('/', Request::path())[1] ? ' active' : '' }}" href="{{ route('teacher.showStudent', $course->id) }}">
            <span>{{ $course->subject->name }}</span>
            <span>{{ $course->classroom->name }}</span>
        </a>
    @endforeach
</nav>
