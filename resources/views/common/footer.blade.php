<footer>
    @if (Auth::user()->admin->first() || Auth::user()->schoolAdmin->first())
        <span>Wizzy untuk Admin</span>
    @elseif (Auth::user()->teacher->first())
        <span>Wizzy untuk Guru</span>
    @elseif (Auth::user()->student->first())
        <span>Wizzy untuk Siswa</span>
    @endif
    <span>&copy; WIZZY 2018. All rights reserved.</span>
</footer>
