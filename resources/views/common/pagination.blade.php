@if ($paginator->lastPage() > 1)
    <div class="pagination">
        @if ($paginator->previousPageUrl())
            <a href="{{ $paginator->previousPageUrl() }}" class="pagination__link pagination__link--arrow">
                <svg width="10" height="10">
                    <use xlink:href="{{ asset('images/icons/arrow.svg#arrow') }}"></use>
                </svg>
            </a>
        @endif
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            @if ($i === $paginator->currentPage())
                <div class="pagination__link pagination__link--active">{{ $i }}</div>
            @else
                <a href="{{ url()->current() . '?page=' . $i }}" class="pagination__link">{{ $i }}</a>
            @endif
        @endfor
        @if ($paginator->nextPageUrl())
            <a href="{{ $paginator->nextPageUrl() }}" class="pagination__link pagination__link--arrow pagination__link--arrow-next">
                <svg width="10" height="10">
                    <use xlink:href="{{ asset('images/icons/arrow.svg#arrow') }}"></use>
                </svg>
            </a>
        @endif
    </div>
@endif