@extends('layout.master')

@section('title')
    Data Sekolah
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="admin">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            <h1>Beranda</h1>
        </div>
        <div class="sadmin__tab sadmin__tab--4">
            <div class="sadmin__tab__item sadmin__tab__item--active"><a href="{{ route('admin.index') }}">Data Sekolah</a></div>
            <div class="sadmin__tab__item"><a href="{{ route('schoolAdmin.class') }}">Daftar Kelas</a></div>
            <div class="sadmin__tab__item"><a href="{{ route('schoolAdmin.subject') }}">Daftar Mata Pelajaran</a></div>
            <div class="sadmin__tab__item"><a href="{{ route('schoolAdmin.shift') }}">Daftar Shift</a></div>
        </div>
        <div class="sadmin__info">
            <div class="sadmin__itable__header">
                <div class="sadmin__itable__icon"></div>
                <div class="sadmin__itable__text"><b>Nama Sekolah</b></div>
                <div class="sadmin__itable__text"><b>{{ $school['name'] }}</b></div>
                <a href="{{ route('schoolAdmin.showUpdateSchool') }}" class=" button button--invert sadmin__info__edit">
                    <span>
                        <svg width="14" height="14">
                            <use xlink:href="{{ asset('images/icons/edit.svg#edit') }}"></use>
                        </svg>
                        Ubah Data
                    </span>
                </a>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/pin.svg#pin') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text">NPSN</div>
                <div class="sadmin__itable__text">{{ $school['npsn'] }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/flag.svg#flag') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text">Jenjang</div>
                <div class="sadmin__itable__text">{{ isset($school['educational_level']) ? $school['educational_level'] : '-' }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/medal.svg#medal') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text">Status</div>
                <div class="sadmin__itable__text">{{ isset($school['type']) ? $school['type'] : '-' }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon antiflex">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/location.svg#location') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text antiflex">Alamat</div>
                <div class="sadmin__itable__text antiflex">
                    <div>{{ $school['address'] }}</div>
                    <div>{{ $school['administrative_village'] }}</div>
                    <div>{{ $school['district'] }}</div>
                    <div>{{ $school['city'] }}</div>
                    <div>{{ $school['province'] }}</div>
                    <div>{{ $school['postal_code'] }}</div>
                </div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/message.svg#message') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text">Email</div>
                <div class="sadmin__itable__text">{{ isset($school['email']) ? $school['email'] : '-' }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/web.svg#web') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text">Website</div>
                <div class="sadmin__itable__text">{{ isset($school['website']) ? $school['website'] : '-' }}</div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/phone.svg#phone') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text">Telepon</div>
                <div class="sadmin__itable__text">{{ isset($school['phone']) ? $school['phone'] : '-' }}</div>
            </div>
            <div class="flex">
                <div class="sadmin__separator"></div>
            </div>
            <div class="flex">
                <div class="sadmin__itable__data sadmin__itable__data--boxed">
                    <div class="sadmin__itable__icon">
                        <svg width="18" height="18">
                            <use xlink:href="{{ asset('images/icons/glasses.svg#glasses') }}"></use>
                        </svg>
                    </div>
                    <div class="sadmin__itable__text">Jumlah Guru</div>
                    <div class="sadmin__itable__text">100</div>
                </div>
                <div class="sadmin__itable__data sadmin__itable__data--boxed">
                    <div class="sadmin__itable__icon">
                        <svg width="18" height="18">
                            <use xlink:href="{{ asset('images/icons/people.svg#people') }}"></use>
                        </svg>
                    </div>
                    <div class="sadmin__itable__text">Jumlah Siswa</div>
                    <div class="sadmin__itable__text">500</div>
                </div>
            </div>
        </div>
    </div>
@endsection
