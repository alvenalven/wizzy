@extends('layout.master')

@section('title')
    Daftar Kelas
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="class">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            <h1>Beranda</h1>
        </div>
        <div class="sadmin__tab sadmin__tab--4">
            <div class="sadmin__tab__item"><a href="{{ route('admin.index') }}">Data Sekolah</a></div>
            <div class="sadmin__tab__item sadmin__tab__item--active"><a href="{{ route('schoolAdmin.class') }}">Daftar Kelas</a></div>
            <div class="sadmin__tab__item"><a href="{{ route('schoolAdmin.subject') }}">Daftar Mata Pelajaran</a></div>
            <div class="sadmin__tab__item"><a href="{{ route('schoolAdmin.shift') }}">Daftar Shift</a></div>
        </div>
        <div class="mart--30 marb--40 ma" style="margin-left: 22px">
            Di halaman ini anda bisa menambahkan daftar kelas yang ada di sekolah.
        </div>
        <div class="sadmin__list__container">
            <div class="sadmin__class">
                <div class="sadmin__list__heading">Daftar Kelas</div>
                <div class="sadmin__list__list">
                    @foreach($classes as $class)
                        <item-list :item="{{ json_encode($class) }}" @edit-item="editItem" @remove-item="removeItem"></item-list>
                    @endforeach
                    @if (count($classes) === 0)
                        <div class="sadmin__list__empty">Belum ada kelas</div>
                    @endif
                </div>
            </div>
        </div>
        <button class="button button--invert sadmin__list__add" @click="showAddModal = true">
            <span>+ Tambah Kelas</span>
        </button>
        <add-class-modal v-if="showAddModal"
                         school-name="{{ Auth::user()->schoolAdmin->first()->school->name }}"
                         @modal-close="showAddModal = false">
        </add-class-modal>
        <edit-class-modal v-if="showEditModal"
                          school-name="{{ Auth::user()->schoolAdmin->first()->school->name }}"
                          :item="item"
                          @modal-close="showEditModal = false">
        </edit-class-modal>
        <delete-class-modal v-if="showDeleteModal"
                            :item="item"
                            @modal-close="showDeleteModal = false">
        </delete-class-modal>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var editIconUrl = '{{ asset('images/icons/edit.svg#edit') }}';
        var removeIconUrl = '{{ asset('images/icons/remove.svg#remove') }}';
        var addUrl = '{{ route('schoolAdmin.addClass') }}';
        var editUrl = '{{ route('schoolAdmin.editClass') }}';
        var deleteUrl = '{{ route('schoolAdmin.deleteClass') }}';
    </script>
    <script src="{{ mix('js/school-admin/class.js') }}"></script>
@endsection
