@extends('layout.master')

@section('title')
    Daftar Mata Pelajaran
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="subject">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            <h1>Beranda</h1>
        </div>
        <div class="sadmin__tab sadmin__tab--4">
            <div class="sadmin__tab__item"><a href="{{ route('admin.index') }}">Data Sekolah</a></div>
            <div class="sadmin__tab__item"><a href="{{ route('schoolAdmin.class') }}">Daftar Kelas</a></div>
            <div class="sadmin__tab__item sadmin__tab__item--active"><a href="{{ route('schoolAdmin.subject') }}">Daftar Mata Pelajaran</a></div>
            <div class="sadmin__tab__item"><a href="{{ route('schoolAdmin.shift') }}">Daftar Shift</a></div>
        </div>
        <div class="mart--30 marb--40 ma" style="margin-left: 22px">
            Di halaman ini anda bisa menambahkan daftar mata pelajaran yang ada di sekolah.
        </div>
        <div class="sadmin__list__container">
            <div class="sadmin__class">
                <div class="sadmin__list__heading">Daftar Mata Pelajaran</div>
                <div class="sadmin__list__list">
                    @foreach($subjects as $subject)
                        <item-list :item="{{ json_encode($subject) }}" @edit-item="editItem" @remove-item="removeItem"></item-list>
                    @endforeach
                    @if (count($subjects) === 0)
                        <div class="sadmin__list__empty">Belum ada mata pelajaran</div>
                    @endif
                </div>
            </div>
        </div>
        <button class="button button--invert sadmin__list__add" @click="showAddModal = true">
            <span>+ Tambah Mata Pelajaran</span>
        </button>
        <add-subject-modal v-if="showAddModal"
                         school-name="{{ Auth::user()->schoolAdmin->first()->school->name }}"
                         @modal-close="showAddModal = false">
        </add-subject-modal>
        <edit-subject-modal v-if="showEditModal"
                          school-name="{{ Auth::user()->schoolAdmin->first()->school->name }}"
                          :item="item"
                          @modal-close="showEditModal = false">
        </edit-subject-modal>
        <delete-subject-modal v-if="showDeleteModal"
                            :item="item"
                            @modal-close="showDeleteModal = false">
        </delete-subject-modal>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var editIconUrl = '{{ asset('images/icons/edit.svg#edit') }}';
        var removeIconUrl = '{{ asset('images/icons/remove.svg#remove') }}';
        var addUrl = '{{ route('schoolAdmin.addSubject') }}';
        var editUrl = '{{ route('schoolAdmin.editSubject') }}';
        var deleteUrl = '{{ route('schoolAdmin.deleteSubject') }}';
    </script>
    <script src="{{ mix('js/school-admin/subject.js') }}"></script>
@endsection
