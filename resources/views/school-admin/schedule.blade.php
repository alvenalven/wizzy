@extends('layout.master')

@section('title')
    Pengelolaan Jadwal {{ $classroom['name'] }}
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="school-admin">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/door.svg#door') }}"></use>
            </svg>
            <h1>Kelas {{ $classroom['name'] }}</h1>
        </div>
        <div class="sadmin__tab">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'schoolAdmin.classroom' ? ' sadmin__tab__item--active' : '' }}"><a href="{{ route('schoolAdmin.classroom', $classroom['id']) }}">Murid</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'schoolAdmin.schedule' ? ' sadmin__tab__item--active' : '' }}"><a href="{{ route('schoolAdmin.schedule', $classroom['id']) }}">Jadwal</a></div>
        </div>
        <div class="sadmin__schedule">
            <div class="sadmin__schedule__shift">
                @foreach ($shifts as $shift)
                    <span>{{ substr($shift->start_time, 0, 5) }} - {{ substr($shift->end_time, 0, 5) }}</span>
                @endforeach
            </div>
            <div class="sadmin__schedule__content">
                <div class="sadmin__schedule__header">
                    <span>Senin</span>
                    <span>Selasa</span>
                    <span>Rabu</span>
                    <span>Kamis</span>
                    <span>Jumat</span>
                    <span>Sabtu</span>
                </div>
                <div class="sadmin__schedule__table">
                    @foreach ($shifts as $index => $shift)
                        <div class="sadmin__schedule__tr">
                            @for ($i = 0; $i < 6; $i++)
                                <div class="sadmin__schedule__td">
                                    <div class="sadmin__td__content">
                                        <?php $count = 0 ?>
                                        @foreach($schedules as $schedule)
                                            @if ($schedule->shift_id == $shift->id && $schedule->day_of_week == $i + 1)
                                                <div class="sadmin__td__edit" @click="editSchedule({{ $schedule->course->subject->id }}, {{ $schedule->course->teacher->id }}, {{ $schedule->id }}, {{ $shift->id }}, {{ $i + 1 }})">
                                                    <svg width="12" height="12">
                                                        <use xlink:href="{{ asset('images/icons/edit.svg#edit') }}"></use>
                                                    </svg>
                                                </div>
                                                <div class="sadmin__td__name">{{ $schedule->course->subject->name }}</div>
                                                <div class="sadmin__td__detail">{{ $schedule->course->teacher->user->first()->name }}</div>
                                                <?php $count += 1 ?>
                                            @endif
                                        @endforeach
                                        @if ($count == 0)
                                            <div class="sadmin__td__blank" @click="addSchedule({{ $shift->id }}, {{ $i + 1 }})">
                                                <div class="sadmin__td__add">
                                                    <svg width="6" height="6">
                                                        <use xlink:href="{{ asset('images/icons/remove.svg#remove') }}"></use>
                                                    </svg>
                                                </div>
                                                <div class="sadmin__td__name">Tambah Jadwal</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endfor
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <modal v-if="showAddModal" @modal-close="showAddModal = false" v-cloak>
            <div class="admin__approve__heading marb--20">Tambah Jadwal</div>
            <form action="{{ route('schoolAdmin.scheduleAdd') }}" method="POST">
                @csrf
                <div class="form__row marb--10">
                    <label for="subject" class="sadmin__label">Nama Mata Pelajaran</label>
                    <select name="subjectId" id="subject" class="sadmin__select">
                        <option disabled selected>Pilih Mata Pelajaran</option>
                        <option :value="subject.id" v-for="subject in subjects">@{{ subject.name }}</option>
                    </select>
                </div>
                <div class="form__row marb--10">
                    <label for="teacher" class="sadmin__label">Nama Guru</label>
                    <select name="teacherId" id="teacher" class="sadmin__select">
                        <option disabled selected>Pilih Guru</option>
                        <option :value="teacher.id" v-for="teacher in teachers">@{{ teacher.user[0].name }}</option>
                    </select>
                </div>
                <input name="classroomId" type="hidden" value="{{ $classroom['id'] }}">
                <input name="shiftId" type="hidden" :value="shiftId">
                <input name="dayOfWeek" type="hidden" :value="dayOfWeek">
                <div class="flex flex--right">
                    <button class="button mart--10" type="submit"><span>Simpan</span></button>
                </div>
            </form>
        </modal>
        <modal v-if="showEditModal" @modal-close="showEditModal = false" v-cloak>
            <div class="admin__approve__heading marb--20">Ubah Jadwal</div>
            <form action="{{ route('schoolAdmin.scheduleEdit') }}" method="POST" style="margin-bottom: -44px;">
                @csrf
                {{ method_field('PATCH') }}
                <div class="form__row marb--10">
                    <label for="subject" class="sadmin__label">Nama Mata Pelajaran</label>
                    <select name="subjectId" id="subject" class="sadmin__select">
                        <option disabled selected>Pilih Mata Pelajaran</option>
                        <option :value="subject.id" v-for="subject in subjects" :selected="subjectId == subject.id">@{{ subject.name }}</option>
                    </select>
                </div>
                <div class="form__row marb--10">
                    <label for="teacher" class="sadmin__label">Nama Guru</label>
                    <select name="teacherId" id="teacher" class="sadmin__select">
                        <option disabled selected>Pilih Guru</option>
                        <option :value="teacher.id" v-for="teacher in teachers" :selected="teacherId == teacher.id">@{{ teacher.user[0].name }}</option>
                    </select>
                </div>
                <input name="classroomId" type="hidden" value="{{ $classroom['id'] }}">
                <input name="shiftId" type="hidden" :value="shiftId">
                <input name="dayOfWeek" type="hidden" :value="dayOfWeek">
                <input name="scheduleId" type="hidden" :value="scheduleId">
                <div class="flex flex--right">
                    <button class="button mart--10" type="submit"><span>Simpan</span></button>
                </div>
            </form>
            <form action="{{ route('schoolAdmin.scheduleDelete') }}" method="POST">
                @csrf
                {{ method_field('DELETE') }}
                <input name="scheduleId" type="hidden" :value="scheduleId">
                <button class="button mart--10" type="submit" style="padding: 10px 20px; margin-right: auto;"><span>Hapus</span></button>
            </form>
        </modal>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var subjects = {!! $classroom->school->subjects !!};
        var teachers = {!! $classroom->school->teachers()->with('user')->get() !!};
    </script>
    <script src="{{ mix('js/school-admin/schedule.js') }}"></script>
@endsection
