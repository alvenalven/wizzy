@extends('layout.master')

@section('title')
    Pengelolaan Kelas {{ $classroom['name'] }}
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="school-admin">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/door.svg#door') }}"></use>
            </svg>
            <h1>Kelas {{ $classroom['name'] }}</h1>
        </div>
        <div class="dashboard__actions">
            {{--<input type="text" class="input" placeholder="Cari murid">--}}
            <button class="button marl--auto" @click="showTable" v-if="!showAddTable"><span>Tambah Siswa</span></button>
        </div>
        <div class="sadmin__tab">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'schoolAdmin.classroom' ? ' sadmin__tab__item--active' : '' }}"><a href="{{ route('schoolAdmin.classroom', $classroom['id']) }}">Murid</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'schoolAdmin.schedule' ? ' sadmin__tab__item--active' : '' }}"><a href="{{ route('schoolAdmin.schedule', $classroom['id']) }}">Jadwal</a></div>
        </div>
        @if (count($students) < 1)
            <span>Belum ada siswa.</span>
        @else
            <student-table :students="{{ json_encode($students->toArray()['data']) }}" :last-page="{{ $students->toArray()['last_page'] }}">
                {{ $students->links('common.pagination') }}
            </student-table>
        @endif

        <search-student-table :classroom-id="{{ $classroom['id'] }}" v-if="showAddTable" @hide-add-table="showAddTable = false"></search-student-table>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var infoUrl = '{{ asset('images/icons/info.svg#info') }}';
        var removeUrl = '{{ asset('images/icons/remove.svg#remove') }}';
        var userIconUrl = '{{ asset('images/icons/user.svg#user') }}';
        var optionIconUrl = '{{ asset('images/icons/option.svg#option') }}';
        var arrowIconUrl = '{{ asset('images/icons/arrow.svg#arrow') }}';
        var schoolNpsn = '{{ Auth::user()->schoolAdmin()->first()->school->npsn }}';
        var studentResendUrl = '{{ route('student.resend') }}';
        var studentCancelUrl = '{{ route('student.cancel') }}';
        var currentPageUrl = '{{ route('schoolAdmin.student') }}';
        var searchStudentUrl = '{{ route('schoolAdmin.studentSearch') }}';
        var addStudentUrl = '{{ route('schoolAdmin.studentAdd') }}';
    </script>
    <script src="{{ mix('js/school-admin/student-class.js') }}"></script>
@endsection
