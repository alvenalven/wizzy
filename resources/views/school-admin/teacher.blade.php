@extends('layout.master')

@section('title')
    Pengelolaan Guru
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="school-admin">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            <h1>Guru</h1>
        </div>
        <div class="dashboard__actions">
            {{--<input type="text" class="input" placeholder="Cari guru">--}}
            <button class="button marl--auto" @click="showTable" v-if="!showAddTable"><span>Tambah Guru</span></button>
        </div>
        <div class="sadmin__tab sadmin__tab--3">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'schoolAdmin.teacher' ? ' sadmin__tab__item--active' : '' }}"><a href="{{ route('schoolAdmin.teacher') }}">Semua</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'schoolAdmin.teacher.active' ? ' sadmin__tab__item--active' : '' }}"><a href="{{ route('schoolAdmin.teacher.active') }}">Aktif</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'schoolAdmin.teacher.pending' ? ' sadmin__tab__item--active' : '' }}"><a href="{{ route('schoolAdmin.teacher.pending') }}">Menunggu</a></div>
        </div>
        @if (count($teachers) < 1)
            <div class="flex" v-if="showGetRemoteDataMsg">
                <span>Belum ada guru. Coba ambil data?</span>
                <a href="#" class="marl--10" @click.prevent="getRemoteData">Ya</a>
                <div class="dashboard__single__loader marl--10" v-if="isGettingRemoteData"></div>
            </div>
            <template v-if="noRemoteDataFound">
                Maaf, data guru untuk sekolah kamu tidak ditemukan. Silakan tambahkan guru secara manual.
            </template>
        @else
            <teacher-table :teachers="{{ json_encode($teachers->toArray()['data']) }}" :last-page="{{ $teachers->toArray()['last_page'] }}">
                {{ $teachers->links('common.pagination') }}
            </teacher-table>
        @endif

        <search-teacher-table :remote-teachers="remoteData" v-if="showAddTable" @hide-add-table="hideAddTable"></search-teacher-table>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var getTeacherUrl = '{{ route('teacher.remote') }}';
        var infoUrl = '{{ asset('images/icons/info.svg#info') }}';
        var removeUrl = '{{ asset('images/icons/remove.svg#remove') }}';
        var schoolNpsn = '{{ Auth::user()->schoolAdmin()->first()->school->npsn }}';
        var inviteTeachersUrl = '{{ route('teacher.invite') }}';
        var currentPageUrl = '{{ route('schoolAdmin.teacher') }}';
        var userIconUrl = '{{ asset('images/icons/user.svg#user') }}';
        var optionIconUrl = '{{ asset('images/icons/option.svg#option') }}';
        var arrowIconUrl = '{{ asset('images/icons/arrow.svg#arrow') }}';
        var teacherResendUrl = '{{ route('teacher.resend') }}';
        var teacherCancelUrl = '{{ route('teacher.cancel') }}';
        var teacherGrantUrl = '{{ route('teacher.grant') }}';
    </script>
    <script src="{{ mix('js/school-admin/teacher.js') }}"></script>
@endsection
