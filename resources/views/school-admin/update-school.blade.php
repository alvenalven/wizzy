@extends('layout.master')

@section('title')
    Ubah Data Sekolah
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="admin">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            <h1>Beranda</h1>
        </div>
        <div class="dashboard__sub__header">
            <a href="{{ route('admin.index') }}" class="flex flex--center marr--30">
                <svg width="15" height="15">
                    <use xlink:href="{{ asset('images/icons/arrow.svg#arrow') }}"></use>
                </svg>
            </a>
            <h2>Ubah Data Sekolah</h2>
        </div>
        <form class="sadmin__info" action="{{ route('schoolAdmin.updateSchool') }}" method="POST">
            <div class="flex">
                <div class="sadmin__separator sadmin__separator--longer sadmin__separator--top"></div>
            </div>
            @csrf
            {{ method_field('PATCH') }}
            <div class="sadmin__itable__header">
                <div class="sadmin__itable__icon"></div>
                <div class="sadmin__itable__text"><b>Nama Sekolah</b></div>
                <div class="sadmin__itable__text"><b>{{ $school['name'] }}</b></div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/pin.svg#pin') }}"></use>
                    </svg>
                </div>
                <label class="sadmin__itable__text">NPSN</label>
                <div class="sadmin__itable__text">
                    <input type="text" class="input" name="educationalLevel" value="{{ $school['npsn'] }}" disabled>
                </div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/flag.svg#flag') }}"></use>
                    </svg>
                </div>
                <label for="educational-level" class="sadmin__itable__text">Jenjang</label>
                <div class="sadmin__itable__text">
                    <input id="educational-level" type="text" class="input" name="educationalLevel" value="{{ $school['educational_level'] }}">
                </div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/medal.svg#medal') }}"></use>
                    </svg>
                </div>
                <label for="status" class="sadmin__itable__text">Status</label>
                <div class="sadmin__itable__text">
                    <input id="status" type="text" class="input" name="type" value="{{ $school['type'] }}">
                </div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon sadmin__itable__icon--padded antiflex">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/location.svg#location') }}"></use>
                    </svg>
                </div>
                <div class="sadmin__itable__text antiflex">
                    <label for="address" class="padt--10 padb--10">Alamat</label>
                    <label for="administrative-village" class="padt--10 padb--10">Desa/Kelurahan</label>
                    <label for="district" class="padt--10 padb--10">Kecamatan</label>
                    <label for="city" class="padt--10 padb--10">Kabupaten/Kota</label>
                    <label for="province" class="padt--10 padb--10">Provinsi</label>
                    <label for="postal-code" class="padt--10 padb--10">Kode Pos</label>
                </div>
                <div class="sadmin__itable__text antiflex">
                    <input id="address" type="text" class="input" name="address" value="{{ $school['address'] }}">
                    <input id="administrative-village" type="text" class="input" name="administrativeVillage" value="{{ $school['administrative_village'] }}">
                    <input id="district" type="text" class="input" name="district" value="{{ $school['district'] }}">
                    <input id="city" type="text" class="input" name="city" value="{{ $school['city'] }}">
                    <input id="province" type="text" class="input" name="province" value="{{ $school['province'] }}">
                    <input id="postal-code" type="text" class="input" name="postalCode" value="{{ $school['postal_code'] }}">
                </div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/message.svg#message') }}"></use>
                    </svg>
                </div>
                <label for="email" class="sadmin__itable__text">Email</label>
                <div class="sadmin__itable__text">
                    <input id="email" type="text" class="input" name="email" value="{{ $school['email'] }}">
                </div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/web.svg#web') }}"></use>
                    </svg>
                </div>
                <label for="website" class="sadmin__itable__text">Website</label>
                <div class="sadmin__itable__text">
                    <input id="website" type="text" class="input" name="website" value="{{ $school['website'] }}">
                </div>
            </div>
            <div class="sadmin__itable__data">
                <div class="sadmin__itable__icon">
                    <svg width="15" height="15">
                        <use xlink:href="{{ asset('images/icons/phone.svg#phone') }}"></use>
                    </svg>
                </div>
                <label for="phone" class="sadmin__itable__text">Telepon</label>
                <div class="sadmin__itable__text">
                    <input id="phone" type="text" class="input" name="phone" value="{{ $school['phone'] }}">
                </div>
            </div>
            <div class="flex">
                <div class="sadmin__separator sadmin__separator--longer"></div>
            </div>
            <button class="button button--invert sadmin__update__button"><span>Simpan Perubahan</span></button>
        </form>
    </div>
@endsection
