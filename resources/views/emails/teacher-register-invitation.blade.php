@component('mail::message')
    Hai, {{ $invitation->user->name }}.

    Kamu diundang untuk menjadi guru di sekolah {{ $school->name }}.
    Berikut ini tautan untuk mengaktivasi akun guru kamu.

@component('mail::panel')
    <a href="{{ route('register.index', $invitation->code) }}">{{ route('register.index', $invitation->code) }}</a>
@endcomponent

Salam hangat,<br>
{{ config('app.name') }}
@endcomponent
