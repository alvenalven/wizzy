@component('mail::message')
    Hai, {{ $invitation->user->name }}.

    Selamat, sekolah kamu berhasil diverifikasi.
    Berikut ini tautan untuk mengaktivasi akun admin kamu.

@component('mail::panel')
    <a href="{{ route('register.index', $invitation->code) }}">{{ route('register.index', $invitation->code) }}</a>
@endcomponent

Salam hangat,<br>
{{ config('app.name') }}
@endcomponent
