@extends('layout.plain')

@section('content')
    <div class="home" id="home" style="width: 100vw; height: 100vh; background: black; display: flex; flex-direction: column; align-items: center; justify-content: center;">
        <div style="font-size: 200px; font-weight: 300;">
            <span style="color:#3d74c5;">W</span>
            <span style="color:#4267b6;">I</span>
            <span style="color:#475aa8;">Z</span>
            <span style="color:#4b4d99;">Z</span>
            <span style="color:#50408a;">Y</span>
        </div>
        <button @click="showModal = true" style="margin-top: 75px; width: 250px; height: 50px; display: flex; align-items: center; justify-content: center; background: none; border-color: white; color: white; cursor: pointer;">Register</button>
        <register-school-modal :show-modal="showModal" @modal-close="showModal = false"></register-school-modal>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var schoolSearchUrl = '{{ route('register.search') }}';
        var schoolIndexUrl = '{{ route('school.index') }}';
        var tickSvgAsset = '{{ asset('images/icons/tick.svg') . '#tick' }}';
        var successImgLink = '{{ asset('images/login/check.png') }}';
    </script>
    <script src="{{ mix('js/home.js') }}"></script>
@endsection
