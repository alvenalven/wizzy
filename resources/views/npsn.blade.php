<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NISN</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
<body style="width: 1300px; margin: auto; display: flex; justify-content: center; padding-top: 40px">
<form method="POST">
    {{ csrf_field() }}
    <input type="text" name="npsn" placeholder="Masukkan NPSN di sini" autofocus required style="padding: 10px 20px; font-size: 25px;">
    <button type="submit" style="padding: 10px 20px; font-size: 25px;">Cari Sekolah</button>
    <div style="margin-top: 10px">Coba pakai <b>20101301</b>, <b>20100217</b>, <b>20101588</b>, <b>11000433</b>, <b>20100188</b>
    @if (isset($npsn) && isset($name) && isset($address) && isset($postalCode)
    && isset($administrativeVillage) && isset($district) && isset($city)
    && isset($province) && isset($status) && isset($schoolHours)
    && isset($educationalLevel) && isset($phone) && isset($email) && isset($website))
        <h1 style="margin-top: 50px;">Data Sekolah:</h1>
        <ul style="font-size: 30px">
            <li>NPSN: <b>{{ $npsn }}</b></li>
            <li>Nama: <b>{{ $name }}</b></li>
            <li>Alamat: <b>{{ $address }}</b></li>
            <li>Kode Pos: <b>{{ $postalCode }}</b></li>
            <li>Desa/Kelurahan: <b>{{ $administrativeVillage }}</b></li>
            <li>Kecamatan: <b>{{ $district }}</b></li>
            <li>Kabupaten/Kota: <b>{{ $city }}</b></li>
            <li>Provinsi: <b>{{ $province }}</b></li>
            <li>Status Sekolah: <b>{{ $status }}</b></li>
            <li>Waktu Sekolah: <b>{{ $schoolHours }}</b></li>
            <li>Jenjang Pendidikan: <b>{{ $educationalLevel }}</b></li>
            <li>Nomor Telepon: <b>{{ $phone }}</b></li>
            <li>Alamat Email: <b>{{ $email }}</b></li>
            <li>Website: <b>{{ $website }}</b></li>
        </ul>
    @endif
</form>
</body>
</html>
