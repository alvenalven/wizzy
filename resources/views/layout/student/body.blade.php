@section('body')
    <div class="dashboard__container">
        <div class="container">
            @section('header')
                @include('common.student.header')
            @show
            <div class="dashboard__student" data-simplebar>
                <div class="dashboard__student__wrapper">
                    @if (Route::currentRouteName() == 'student.index')
                        @include('common.student.profile-index')
                    @else
                        @include('common.student.profile-common')
                    @endif
                    @yield('content')
                </div>
            </div>
            @section('footer')
                @include('common.footer')
            @show
        </div>
    </div>
@show
