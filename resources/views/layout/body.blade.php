@section('body')
    <div class="dashboard__container">
        <div class="container">
            @section('header')
                @include('common.header')
            @show
            <div class="dashboard">
                <div class="dashboard__panel" data-simplebar>
                    <div class="dashboard__profile">
                        {{--<img src="{{ asset('images/profile.jpg') }}" class="dashboard__profile__image">--}}
                        <div class="dashboard__profile__empty marb--20">
                            <svg width="74" height="74">
                                <use xlink:href="{{ asset('images/icons/user.svg#user') }}"></use>
                            </svg>
                        </div>
                        <div class="dashboard__profile__name">{{ Auth::user()->name }}</div>
                    </div>
                    @section('navigation')
                        @if (Auth::user()->admin->first())
                            @include('common.navigation-admin')
                        @elseif (Auth::user()->schoolAdmin->first())
                            @include('common.navigation-school-admin')
                        @elseif (Auth::user()->teacher->first())
                            @include('common.navigation-teacher')
                        @endif
                    @show
                </div>
                <div class="dashboard__content" data-simplebar>
                    @yield('content')
                </div>
            </div>
            @section('footer')
                @include('common.footer')
            @show
        </div>
    </div>
@show
