@extends('layout.master')

@section('title')
    Daftar Sekolah
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="admin">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            <h1>Daftar Sekolah</h1>
        </div>
        <h2 class="dashboard__sub__header">Daftar Sekolah yang telah bergabung</h2>
        <school-list-approved :schools="{{ json_encode($schools->toArray()['data']) }}"></school-list-approved>
        {{ $schools->links('common.pagination') }}
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var arrowUrl = '{{ asset('images/icons/arrow.svg#arrow') }}';
        var flagUrl = '{{ asset('images/icons/flag.svg#flag') }}';
        var locationUrl = '{{ asset('images/icons/location.svg#location') }}';
        var messageUrl = '{{ asset('images/icons/message.svg#message') }}';
        var phoneUrl = '{{ asset('images/icons/phone.svg#phone') }}';
        var userUrl = '{{ asset('images/icons/user.svg#user') }}';
        var medalUrl = '{{ asset('images/icons/medal.svg#medal') }}';
        var webUrl = '{{ asset('images/icons/web.svg#web') }}';
        var approveUrl = '{{ route('admin.approve', '') }}';
        var disapproveUrl = '{{ route('admin.disapprove', '') }}';
    </script>
    <script src="{{ mix('js/admin.js') }}"></script>
@endsection