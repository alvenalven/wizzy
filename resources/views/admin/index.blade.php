@extends('layout.master')

@section('title')
    Beranda
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="admin">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            <h1>Beranda</h1>
        </div>
        <h2 class="dashboard__sub__header">Permintaan Tertunda</h2>
        <school-list :schools="{{ json_encode($schools->toArray()['data']) }}"></school-list>
        {{ $schools->links('common.pagination') }}
    </div>
    {{--{{ Session::get('status') }}--}}
@endsection

@section('scripts')
    @parent
    <script>
        var arrowUrl = '{{ asset('images/icons/arrow.svg#arrow') }}';
        var flagUrl = '{{ asset('images/icons/flag.svg#flag') }}';
        var locationUrl = '{{ asset('images/icons/location.svg#location') }}';
        var messageUrl = '{{ asset('images/icons/message.svg#message') }}';
        var phoneUrl = '{{ asset('images/icons/phone.svg#phone') }}';
        var userUrl = '{{ asset('images/icons/user.svg#user') }}';
        var medalUrl = '{{ asset('images/icons/medal.svg#medal') }}';
        var webUrl = '{{ asset('images/icons/web.svg#web') }}';
        var approveUrl = '{{ route('admin.approve', '') }}';
        var disapproveUrl = '{{ route('admin.disapprove', '') }}';
    </script>
    <script src="{{ mix('js/admin.js') }}"></script>
@endsection