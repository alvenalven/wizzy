<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NISN</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
<body style="width: 1300px; margin: auto; display: flex; justify-content: center; padding-top: 200px">
<form method="POST">
    {{ csrf_field() }}
    <input type="text" name="nisn" placeholder="Masukkan NISN di sini" autofocus required style="padding: 10px 20px; font-size: 25px;">
    <button type="submit" style="padding: 10px 20px; font-size: 25px;">Cari Pelajar</button>
    <div style="margin-top: 10px">Coba pakai <b>9961994650</b>, <b>0013638916</b>, <b>9961994601</b>, <b>9961994602</b>, <b>9993346036</b>, <b>0012013528</b>, <b>9956635927</b>, <b>0030072987</b></div>
    @if (isset($name) && isset($gender) && isset($bornPlace) && isset($bornDate))
        <h1 style="margin-top: 50px;">Data Pelajar:</h1>
        <ul style="font-size: 30px">
            <li>Nama: <b>{{ $name }}</b></li>
            <li>Jenis Kelamin: <b>{{ $gender }}</b></li>
            <li>Tempat Lahir: <b>{{ $bornPlace }}</b></li>
            <li>Tanggal Lahir: <b>{{ $bornDate }}</b></li>
        </ul>
    @endif
</form>
</body>
</html>
