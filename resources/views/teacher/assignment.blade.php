@extends('layout.master')

@section('title')
    Tugas Pelajaran {{ $course->subject->name }} {{ $course->classroom->name }}
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="teacher">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            <h1>Pelajaran {{ $course->subject->name }} {{ $course->classroom->name }}</h1>
        </div>
        <div class="dashboard__actions">
            <button class="button marl--auto" @click="showAddModal = true"><span>Tambah Tugas</span></button>
        </div>
        <div class="sadmin__tab sadmin__tab--4">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showStudent' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showStudent', $course->id) }}">Murid</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showMaterial' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showMaterial', $course->id) }}">Materi</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showAssignment' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showAssignment', $course->id) }}">Tugas</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showGradeInput' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showGradeInput', $course->id) }}">Nilai</a></div>
        </div>
        @if (count($assignments) > 0)
            <table class="table teacher__assignment__table">
                <thead>
                <tr>
                    <th>Nama Tugas</th>
                    <th>Mulai</th>
                    <th>Deadline</th>
                    <th>Pengumpulan</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($assignments as $assignment)
                    <tr>
                        <td>{{ $assignment->name }}</td>
                        <td>{{ $assignment->start_date->formatLocalized('%d %B %Y') }}</td>
                        <td>{{ $assignment->end_date->formatLocalized('%d %B %Y') }}</td>
                        <td>
                            @if (\Carbon\Carbon::now()->gte($assignment->start_date))
                                <span class="underlined pointer"
                                      @click="viewSubmissions({{ $assignment }}, {{ $assignment->course->classroom->students()->with('user')->with(['assignmentSubmissions' => function ($query) use ($assignment) { $query->where('assignment_id', $assignment->id)->first(); }])->get() }}, {{ \Carbon\Carbon::now()->gte($assignment->start_date) && \Carbon\Carbon::now()->lt($assignment->end_date) ? true : false }})">{{ count($assignment->submissions) }}
                                    /{{ count($assignment->course->classroom->students) }}</span>
                            @else
                                <span>-</span>
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('teacher.downloadAssignment') }}" method="POST">
                                @csrf
                                <input type="hidden" name="assignmentId" value="{{ $assignment->id }}">
                                <button type="submit" class="teacher__action__button">
                                    <svg width="17" height="17">
                                        <use xlink:href="{{ asset('images/icons/download.svg#download') }}"></use>
                                    </svg>
                                </button>
                            </form>
                        </td>
                        <td>
                            <div class="teacher__action__button" @click="editAssignment({{ $assignment }})">
                                <svg width="17" height="17">
                                    <use xlink:href="{{ asset('images/icons/edit.svg#edit') }}"></use>
                                </svg>
                            </div>
                        </td>
                        <td>
                            <div class="teacher__action__button" @click="deleteAssignment({{ $assignment }})">
                                <svg width="17" height="17">
                                    <use xlink:href="{{ asset('images/icons/remove.svg#remove') }}"></use>
                                </svg>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <span>Belum ada tugas.</span>
        @endif
        <modal v-if="showDeleteModal" @modal-close="showDeleteModal = false" v-cloak>
            <div class="admin__approve">
                <div class="admin__approve__heading">Hapus Tugas</div>
                <div class="admin__school__name">@{{ assignment.name }}</div>
                <div class="admin__confirm__text text--center">Anda Yakin?</div>
                <form action="{{ route('teacher.deleteAssignment') }}" method="POST">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="assignmentId" :value="assignment.id">
                    <div class="flex flex--center mart--10">
                        <button type="submit" class="button marr--20"><span>Ya</span></button>
                        <button class="button" @click.prevent="showDeleteModal = false"><span>Tidak</span></button>
                    </div>
                </form>
            </div>
        </modal>
        <modal v-if="showAddModal" @modal-close="showAddModal = false" v-cloak>
            <div class="admin__approve">
                <div class="admin__approve__heading">Tambah Tugas</div>
                <div class="admin__school__name">{{ $course->subject->name }} {{ $course->classroom->name }}</div>
                <form action="{{ route('teacher.uploadAssignment') }}" method="POST" enctype="multipart/form-data"
                      class="teacher__form">
                    @csrf
                    <div class="form__row marb--10">
                        <label for="assignment-name" class="sadmin__label">Nama Tugas</label>
                        <input type="text" class="input" name="name" id="assignment-name">
                    </div>
                    <div class="form__row marb--10">
                        <label for="assignment-file" class="sadmin__label">Upload Dokumen</label>
                        <input type="file" name="assignment" id="assignment-file">
                    </div>
                    <div class="form__row marb--10">
                        <label for="start-date" class="sadmin__label">Tugas Mulai</label>
                        <input type="date" name="startDate" id="start-date">
                    </div>
                    <div class="form__row marb--10">
                        <label for="end-date" class="sadmin__label">Tugas Selesai</label>
                        <input type="date" name="endDate" id="end-date">
                    </div>
                    <input type="hidden" name="courseId" value="{{ $course->id }}">
                    <div class="flex flex--center mart--30">
                        <button type="submit" class="button"><span>Simpan</span></button>
                    </div>
                </form>
            </div>
        </modal>
        <modal v-if="showEditModal" @modal-close="showEditModal = false" v-cloak>
            <div class="admin__approve__heading">Ubah Waktu</div>
            <div class="admin__school__name">@{{ assignment.name }}</div>
            <form action="{{ route('teacher.editAssignment') }}" method="POST" class="teacher__form">
                @csrf
                {{ method_field('PATCH') }}
                <div class="form__row marb--10">
                    <label for="start-date1" class="sadmin__label">Tugas Mulai</label>
                    <input type="date" name="startDate" id="start-date1">
                </div>
                <div class="form__row marb--10">
                    <label for="end-date1" class="sadmin__label">Tugas Selesai</label>
                    <input type="date" name="endDate" id="end-date1">
                </div>
                <input type="hidden" name="assignmentId" :value="assignment.id">
                <div class="flex flex--center mart--30">
                    <button type="submit" class="button"><span>Simpan</span></button>
                </div>
            </form>
        </modal>
        <modal v-if="showSubmissionsModal" @modal-close="showSubmissionsModal = false" :width="750" v-cloak>
            <div class="admin__approve__heading">Pengumpulan Tugas</div>
            <div class="admin__school__name">@{{ assignment.name }}</div>
            <div class="teacher__assignment__status teacher__assignment__status--green" v-if="isInProgress">Sedang
                Berjalan
            </div>
            <div class="teacher__assignment__status teacher__assignment__status--red" v-else>Sudah Berakhir</div>
            <table class="table teacher__asubmission__table">
                <thead>
                <tr>
                    <th>Nama Siswa</th>
                    <th>Waktu Pengumpulan</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="student in students"
                    :class="{ 'teacher__row__danger' : !student.assignment_submissions[0] && !isInProgress }">
                    <td>@{{ student.user[0].name }}</td>
                    <td>
                        <template v-if="student.assignment_submissions[0]">
                            <div class="text--green">@{{ student.assignment_submissions[0].created_at }}</div>
                        </template>
                        <template v-else>-</template>
                    </td>
                    <td>
                        <form action="{{ route('teacher.downloadAssignmentSubmission') }}" method="POST"
                              v-if="student.assignment_submissions[0]">
                            @csrf
                            <input type="hidden" name="assignmentSubmissionId"
                                   :value="student.assignment_submissions[0].id">
                            <button type="submit" class="teacher__action__button">
                                <svg width="15" height="15">
                                    <use xlink:href="{{ asset('images/icons/download.svg#download') }}"></use>
                                </svg>
                            </button>
                        </form>
                    </td>
                </tr>
                </tbody>
            </table>
        </modal>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{{ mix('js/teacher/assignment.js') }}"></script>
@endsection
