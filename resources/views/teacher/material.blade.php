@extends('layout.master')

@section('title')
    Materi Pelajaran {{ $course->subject->name }} {{ $course->classroom->name }}
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="teacher">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            <h1>Pelajaran {{ $course->subject->name }} {{ $course->classroom->name }}</h1>
        </div>
        <div class="dashboard__actions">
            <button class="button marl--auto" @click="showAddModal = true"><span>Tambah Materi</span></button>
        </div>
        <div class="sadmin__tab sadmin__tab--4">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showStudent' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showStudent', $course->id) }}">Murid</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showMaterial' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showMaterial', $course->id) }}">Materi</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showAssignment' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showAssignment', $course->id) }}">Tugas</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showGradeInput' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showGradeInput', $course->id) }}">Nilai</a></div>
        </div>
        @if (count($materials) > 0)
            <table class="table teacher__material__table">
                <thead>
                <tr>
                    <th></th>
                    <th>Nama Materi</th>
                    <th>Tanggal Upload</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($materials as $material)
                        <tr>
                            <td>
                                @if (pathinfo($material->file)['extension'] == 'doc'
                                    || pathinfo($material->file)['extension'] == 'docx'
                                    || pathinfo($material->file)['extension'] == 'docm'
                                    || pathinfo($material->file)['extension'] == 'docb')
                                    <svg width="30" height="30">
                                        <use xlink:href="{{ asset('images/icons/doc.svg#doc') }}"></use>
                                    </svg>
                                @elseif (pathinfo($material->file)['extension'] == 'pdf')
                                    <svg width="30" height="30">
                                        <use xlink:href="{{ asset('images/icons/pdf.svg#pdf') }}"></use>
                                    </svg>
                                @elseif (pathinfo($material->file)['extension'] == 'ppt'
                                    || pathinfo($material->file)['extension'] == 'pptx'
                                    || pathinfo($material->file)['extension'] == 'pptm'
                                    || pathinfo($material->file)['extension'] == 'pps'
                                    || pathinfo($material->file)['extension'] == 'ppsx'
                                    || pathinfo($material->file)['extension'] == 'ppsm')
                                    <svg width="30" height="30">
                                        <use xlink:href="{{ asset('images/icons/ppt.svg#ppt') }}"></use>
                                    </svg>
                                @endif
                            </td>
                            <td>{{ $material->name }}</td>
                            <td>{{ $material->created_at->formatLocalized('%d %B %Y') }}</td>
                            <td>
                                <form action="{{ route('teacher.downloadMaterial') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="materialId" value="{{ $material->id }}">
                                    <button type="submit" class="teacher__action__button">
                                        <svg width="17" height="17">
                                            <use xlink:href="{{ asset('images/icons/download.svg#download') }}"></use>
                                        </svg>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <div class="teacher__action__button" @click="deleteMaterial({{ $material }})">
                                    <svg width="17" height="17">
                                        <use xlink:href="{{ asset('images/icons/remove.svg#remove') }}"></use>
                                    </svg>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <span>Belum ada materi.</span>
        @endif
        <modal v-if="showDeleteModal" @modal-close="showDeleteModal = false" v-cloak>
            <div class="admin__approve">
                <div class="admin__approve__heading">Hapus Materi</div>
                <div class="admin__school__name">@{{ material.name }}</div>
                <div class="admin__confirm__text text--center">Anda Yakin?</div>
                <form action="{{ route('teacher.deleteMaterial') }}" method="POST">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="materialId" :value="material.id">
                    <div class="flex flex--center mart--10">
                        <button type="submit" class="button marr--20"><span>Ya</span></button>
                        <button class="button" @click.prevent="showDeleteModal = false"><span>Tidak</span></button>
                    </div>
                </form>
            </div>
        </modal>
        <modal v-if="showAddModal" @modal-close="showAddModal = false" v-cloak>
            <div class="admin__approve">
                <div class="admin__approve__heading">Tambah Materi</div>
                <div class="admin__school__name">{{ $course->subject->name }} {{ $course->classroom->name }}</div>
                <form action="{{ route('teacher.addMaterial') }}" method="POST" enctype="multipart/form-data"
                      class="teacher__form">
                    @csrf
                    <div class="form__row marb--10">
                        <label for="material-name" class="sadmin__label">Nama Materi</label>
                        <input type="text" class="input" name="name" id="material-name">
                    </div>
                    <div class="form__row marb--10">
                        <label for="material-file" class="sadmin__label">Upload Dokumen</label>
                        <input type="file" name="material" id="material-file">
                    </div>
                    <input type="hidden" name="courseId" value="{{ $course->id }}">
                    <div class="flex flex--center mart--30">
                        <button type="submit" class="button"><span>Simpan</span></button>
                    </div>
                </form>
            </div>
        </modal>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{{ mix('js/teacher/material.js') }}"></script>
@endsection
