@extends('layout.plain')

@section('content')
    <div class="logadmin">
        <div class="logadmin__illustration">WIZZY</div>
        <div class="logadmin__box">
            <h1>Login Guru</h1>
            <form action="{{ route('teacher.login.auth') }}" method="POST">
                @csrf
                <div class="hform__row{{ $errors->has('username') ? ' hform__row--error' : '' }}{{ old('username') && !$errors->has('username') ? ' hform__row--success' : '' }}">
                    <label class="hlabel" for="username">Username/Email</label>
                    <input class="hinput" id="username" name="username" type="text" value="{{ old('username') }}"
                           autofocus>
                    <span class="error">{{ $errors->first('username') }}</span>
                </div>
                <div class="hform__row{{ $errors->has('password') ? ' hform__row--error' : ''}}">
                    <label class="hlabel" for="password">Password</label>
                    <input class="hinput" id="password" name="password" type="password">
                    <span class="error">{{ $errors->first('password') }}</span>
                </div>
                <div class="hform__row hform__row--center">
                    <button class="hbutton" type="submit"><span>Masuk</span></button>
                </div>
            </form>
        </div>
    </div>
@endsection
