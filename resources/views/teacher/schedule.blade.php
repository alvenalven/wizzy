@extends('layout.master')

@section('title')
    Jadwal Mengajar
@endsection

@section('content')
    <div class="dashboard__content__wrapper">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/time.svg#time') }}"></use>
            </svg>
            <h1>Jadwal Mengajar</h1>
        </div>
        <div class="sadmin__schedule mart--40">
            <div class="sadmin__schedule__shift">
                @foreach ($shifts as $shift)
                    <span>{{ substr($shift->start_time, 0, 5) }} - {{ substr($shift->end_time, 0, 5) }}</span>
                @endforeach
            </div>
            <div class="sadmin__schedule__content">
                <div class="sadmin__schedule__header">
                    <span>Senin</span>
                    <span>Selasa</span>
                    <span>Rabu</span>
                    <span>Kamis</span>
                    <span>Jumat</span>
                    <span>Sabtu</span>
                </div>
                <div class="sadmin__schedule__table">
                    @foreach ($shifts as $index => $shift)
                        <div class="sadmin__schedule__tr">
                            @for ($i = 0; $i < 6; $i++)
                                <div class="sadmin__schedule__td">
                                    <div class="sadmin__td__content">
                                        <?php $count = 0 ?>
                                        @foreach($schedules as $schedule)
                                            @if ($schedule->shift_id == $shift->id && $schedule->day_of_week == $i + 1)
                                                <div class="sadmin__td__name">{{ $schedule->course->subject->name }}</div>
                                                <div class="sadmin__td__detail">{{ $schedule->course->classroom->name }}</div>
                                                <?php $count += 1 ?>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endfor
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
