@extends('layout.master')

@section('title')
    Murid Pelajaran {{ $course->subject->name }} {{ $course->classroom->name }}
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="teacher">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            <h1>Pelajaran {{ $course->subject->name }} {{ $course->classroom->name }}</h1>
        </div>
        <div class="sadmin__tab sadmin__tab--4">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showStudent' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showStudent', $course->id) }}">Murid</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showMaterial' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showMaterial', $course->id) }}">Materi</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showAssignment' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showAssignment', $course->id) }}">Tugas</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showGradeInput' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showGradeInput', $course->id) }}">Nilai</a></div>
        </div>
        @if (count($students) < 1)
            <span>Belum ada siswa.</span>
        @else
            <student-table :students="{{ json_encode($students->toArray()['data']) }}"
                           :last-page="{{ $students->toArray()['last_page'] }}">
                {{ $students->links('common.pagination') }}
            </student-table>
        @endif
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        var infoUrl = '{{ asset('images/icons/info.svg#info') }}';
        var removeUrl = '{{ asset('images/icons/remove.svg#remove') }}';
        var userIconUrl = '{{ asset('images/icons/user.svg#user') }}';
        var optionIconUrl = '{{ asset('images/icons/option.svg#option') }}';
        var arrowIconUrl = '{{ asset('images/icons/arrow.svg#arrow') }}';
        var studentResendUrl = '{{ route('student.resend') }}';
        var studentCancelUrl = '{{ route('student.cancel') }}';
        var currentPageUrl = '{{ route('schoolAdmin.student') }}';
    </script>
    <script src="{{ mix('js/teacher/student.js') }}"></script>
@endsection
