@extends('layout.master')

@section('title')
    Nilai Pelajaran {{ $course->subject->name }} {{ $course->classroom->name }}
@endsection

@section('content')
    <div class="dashboard__content__wrapper" id="teacher">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/clipboard.svg#clipboard') }}"></use>
            </svg>
            <h1>Nilai {{ $course->subject->name }} {{ $course->classroom->name }}</h1>
        </div>
        <div class="sadmin__tab sadmin__tab--4">
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showStudent' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showStudent', $course->id) }}">Murid</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showMaterial' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showMaterial', $course->id) }}">Materi</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showAssignment' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showAssignment', $course->id) }}">Tugas</a></div>
            <div class="sadmin__tab__item{{ Route::currentRouteName() == 'teacher.showGradeInput' ? ' sadmin__tab__item--active' : '' }}">
                <a href="{{ route('teacher.showGradeInput', $course->id) }}">Nilai</a></div>
        </div>
        <div class="marl--30">
            @if (!$hasFilledGrade)
                @if (count($students) > 0)
                    <div class="sadmin__grade__heading">
                        <span class="text--bold">NISN</span>
                        <span class="text--bold">Nama</span>
                        <span class="text--bold">Nilai Tengah Semester</span>
                        <span class="text--bold">Nilai Akhir</span>
                    </div>
                    <form action="{{ route('teacher.inputGrade') }}" method="POST">
                        @csrf
                        @foreach($students as $student)
                            <div class="sadmin__grade">
                                <span>{{ $student->nisn }}</span>
                                <span>{{ $student->user()->first()->name }}</span>
                                <input type="text" class="input" name="mid[{{ $student->id }}]">
                                <input type="text" class="input" name="final[{{ $student->id }}]">
                            </div>
                        @endforeach
                        <input type="hidden" value="{{ $course->id }}" name="courseId">
                        <div class="flex flex--right">
                            <button type="submit" class="button mart--30" style="margin-right: 80px;">
                                <span>Simpan</span>
                            </button>
                        </div>
                    </form>
                @else
                    Belum ada murid.
                @endif
            @else
                Nilai sudah diberikan ke semua murid untuk mata pelajaran ini.
            @endif
        </div>
    </div>
@endsection