@extends('layout.master')

@section('title')
    Beranda Guru
@endsection

@section('content')
    <div class="dashboard__content__wrapper">
        <div class="dashboard__header">
            <svg width="26" height="26">
                <use xlink:href="{{ asset('images/icons/home.svg#home') }}"></use>
            </svg>
            <h1>Beranda</h1>
        </div>
        <div class="teacher__columns">
            <div class="teacher__column__left">
                <div class="teacher__stats">
                    <div class="teacher__stats__box">
                        <div class="teacher__stats__name">Kelas yang diajar</div>
                        <div class="teacher__stats__amount">{{ count(Auth::user()->teacher->first()->courses) }}</div>
                    </div>
                    <div class="teacher__stats__box">
                        <div class="teacher__stats__name">Tugas yang telah diberikan</div>
                        <div class="teacher__stats__amount">{{ $totalAssignment }}</div>
                    </div>
                    <div class="teacher__stats__box">
                        <div class="teacher__stats__name">Tugas yang sedang berjalan</div>
                        <div class="teacher__stats__amount">{{ $totalOngoingAssignment }}</div>
                    </div>
                </div>
                <div class="teacher__dashboard__box">
                    <div class="teacher__dbox__header">Jadwal</div>
                    <div class="teacher__dbox__content">
                        @if ($currentSchedule)
                            <div class="teacher__dbox__cheader">
                                Jadwal Anda Sekarang
                            </div>
                            <div class="teacher__dbox__schedule teacher__dbox__schedule--active">
                                <div class="teacher__dbox__sname">{{ $currentSchedule->course->subject->name }} {{ $currentSchedule->course->classroom->name }}</div>
                                <div class="teacher__dbox__sshift">{{ substr($currentSchedule->shift->start_time, 0, 5) }} - {{ substr($currentSchedule->shift->end_time, 0, 5) }}</div>
                            </div>
                        @endif
                        @if (count($nextSchedule) > 0)
                            <div class="teacher__dbox__cheader{{ $currentSchedule ? ' mart--30' : '' }}">
                                Jadwal Anda Berikutnya
                            </div>
                            @foreach ($nextSchedule as $schedule)
                                <div class="teacher__dbox__schedule">
                                    <div class="teacher__dbox__sname">{{ $schedule->course->subject->name }} {{ $schedule->course->classroom->name }}</div>
                                    <div class="teacher__dbox__sshift">{{ substr($schedule->shift->start_time, 0, 5) }} - {{ substr($schedule->shift->end_time, 0, 5) }}</div>
                                </div>
                            @endforeach
                        @endif
                        @if (!$currentSchedule && count($nextSchedule) == 0)
                            <div class="teacher__dbox__cheader no-margin">
                                Tidak ada jadwal untuk hari ini.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="teacher__column__right">
                <div class="teacher__dashboard__box teacher__dashboard__box--red">
                    <div class="teacher__dbox__header">Perhatikan</div>
                    <div class="teacher__dbox__content teacher__dbox__content--side">
                        @foreach($problematicStudents as $student)
                            <div class="teacher__prstd">
                                <div class="teacher__prstd__icon">
                                    <svg width="17" height="17" style="fill: #b30000;">
                                        <use xlink:href="{{ asset('images/icons/info.svg#info') }}"></use>
                                    </svg>
                                </div>
                                <div class="teacher__prstd__desc">
                                    <span>{{ $student->user()->first()->name }}</span>
                                    <span>{{ $student->problematic_course->subject->name }} {{ $student->problematic_course->classroom->name }}</span>
                                    <span>tidak mengerjakan tugas {{ $student->problematic_assignment->name }}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
