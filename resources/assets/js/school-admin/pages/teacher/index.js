import SearchTeacherTable from './components/SearchTeacherTable.vue'
import TeacherTable from './components/TeacherTable.vue'

new Vue({
    el: '#school-admin',
    data: {
        showAddTable: false,
        remoteData: [{}],
        isGettingRemoteData: false,
        showGetRemoteDataMsg: true,
        noRemoteDataFound: false
    },
    components: {
        SearchTeacherTable,
        TeacherTable
    },
    methods: {
        getRemoteData() {
            this.isGettingRemoteData = true;
            axios.post(getTeacherUrl, {
                key: schoolNpsn,
                limit: 999
            })
                .then(response => {
                    this.isGettingRemoteData = false;
                    this.showGetRemoteDataMsg = false;
                    if (response.data.length > 0) {
                        this.remoteData = response.data;
                        this.remoteData.forEach((teacher) => {
                            teacher.code = teacher.nuptk ? teacher.nuptk : teacher.ptk_id;
                        });
                        this.showAddTable = true;
                    }
                    else {
                        this.noRemoteDataFound = true;
                    }
                });
        },
        showTable() {
            this.showGetRemoteDataMsg = false;
            this.showAddTable = true;
        },
        hideAddTable() {
            this.showAddTable = false;
            this.remoteData = [{}];
        }
    }
});
