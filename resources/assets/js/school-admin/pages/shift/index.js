import ShiftList from './components/ShiftList.vue'
import AddShiftModal from './components/AddShiftModal.vue'
import EditShiftModal from './components/EditShiftModal.vue'
import DeleteShiftModal from './components/DeleteShiftModal.vue'

new Vue({
    el: '#subject',
    components: {
        ShiftList,
        AddShiftModal,
        EditShiftModal,
        DeleteShiftModal,
    },
    data: {
        showAddModal: false,
        showEditModal: false,
        showDeleteModal: false,
        item: {}
    },
    methods: {
        editItem(value) {
            this.item = value;
            this.showEditModal = true;
        },
        removeItem(value) {
            this.item = value;
            this.showDeleteModal = true;
        }
    }
});
