import ItemList from './../../components/ItemList.vue'
import AddClassModal from './components/AddClassModal.vue'
import EditClassModal from './components/EditClassModal.vue'
import DeleteClassModal from './components/DeleteClassModal.vue'

new Vue({
    el: '#class',
    components: {
        ItemList,
        AddClassModal,
        EditClassModal,
        DeleteClassModal,
    },
    data: {
        showAddModal: false,
        showEditModal: false,
        showDeleteModal: false,
        item: {}
    },
    methods: {
        editItem(value) {
            this.item = value;
            this.showEditModal = true;
        },
        removeItem(value) {
            this.item = value;
            this.showDeleteModal = true;
        }
    }
});
