import ItemList from './../../components/ItemList.vue'
import AddSubjectModal from './components/AddSubjectModal.vue'
import EditSubjectModal from './components/EditSubjectModal.vue'
import DeleteSubjectModal from './components/DeleteSubjectModal.vue'

new Vue({
    el: '#subject',
    components: {
        ItemList,
        AddSubjectModal,
        EditSubjectModal,
        DeleteSubjectModal,
    },
    data: {
        showAddModal: false,
        showEditModal: false,
        showDeleteModal: false,
        item: {}
    },
    methods: {
        editItem(value) {
            this.item = value;
            this.showEditModal = true;
        },
        removeItem(value) {
            this.item = value;
            this.showDeleteModal = true;
        }
    }
});
