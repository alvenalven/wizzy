new Vue({
    el: '#school-admin',
    data: {
        subjects: subjects,
        teachers: teachers,
        showAddModal: false,
        showEditModal: false,
        shiftId: Number,
        dayOfWeek: Number,
        subjectId: Number,
        teacherId: Number,
        scheduleId: Number
    },
    methods: {
        addSchedule(shiftId, dayOfWeek) {
            this.shiftId = shiftId;
            this.dayOfWeek = dayOfWeek;
            this.showAddModalPopup();
        },
        editSchedule(subjectId, teacherId, scheduleId, shiftId, dayOfWeek) {
            this.shiftId = shiftId;
            this.dayOfWeek = dayOfWeek;
            this.subjectId = subjectId;
            this.teacherId = teacherId;
            this.scheduleId = scheduleId;
            this.showEditModalPopup();
        },
        showAddModalPopup() {
            this.showAddModal = true;
        },
        showEditModalPopup() {
            this.showEditModal = true;
        },
    }
});
