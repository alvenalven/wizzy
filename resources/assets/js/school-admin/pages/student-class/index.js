import SearchStudentTable from './components/SearchStudentTable.vue'
import StudentTable from './components/StudentTable.vue'

new Vue({
    el: '#school-admin',
    data: {
        showAddTable: false
    },
    components: {
        SearchStudentTable,
        StudentTable,
    },
    methods: {
        showTable() {
            this.showAddTable = true;
        }
    }
});
