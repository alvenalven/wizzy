import SchoolList from './components/SchoolList.vue'
import SchoolListApproved from './components/SchoolListApproved.vue'

new Vue({
    el: '#admin',
    components: {
        SchoolList,
        SchoolListApproved
    }
});
