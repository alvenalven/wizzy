import RegisterSchoolModal from './components/RegisterSchoolModal.vue'

new Vue({
    el: '#home',
    components: {
        RegisterSchoolModal
    },
    data: {
        showModal: false
    }
});
