new Vue({
    el: '#teacher',
    data: {
        showDeleteModal: false,
        showAddModal: false,
        showEditModal: false,
        showSubmissionsModal: false,
        assignment: {},
        students: [],
        isInProgress: null
    },
    methods: {
        deleteAssignment(assignment) {
            this.assignment = assignment;
            this.showDeleteModal = true;
        },
        editAssignment(assignment) {
            this.assignment = assignment;
            this.showEditModal = true;
        },
        viewSubmissions(assignment, students, isInProgress) {
            this.assignment = assignment;
            this.students = students;
            this.isInProgress = isInProgress;
            this.showSubmissionsModal = true;
        }
    }
});
