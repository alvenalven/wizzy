import StudentTable from './components/StudentTable.vue'

new Vue({
    el: '#teacher',
    data: {
        showAddTable: false
    },
    components: {
        StudentTable,
    },
});
