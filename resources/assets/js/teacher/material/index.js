new Vue({
    el: '#teacher',
    data: {
        showDeleteModal: false,
        showAddModal: false,
        material: {},
    },
    methods: {
        deleteMaterial(material) {
            this.material = material;
            this.showDeleteModal = true;
        }
    }
});
